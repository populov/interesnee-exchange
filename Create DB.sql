﻿CREATE TABLE [dbo].[Currencies] (
    [curID] INT           IDENTITY (1, 1) NOT NULL CONSTRAINT PK_Currencies_curID PRIMARY KEY NONCLUSTERED,
    [code]  NCHAR (3)     NOT NULL,
    [name]  NVARCHAR (50) NOT NULL
);

CREATE TABLE [dbo].[Rates] (
    [src]  INT           NOT NULL CONSTRAINT FK_Rates_src FOREIGN KEY REFERENCES [dbo].[Currencies] ([curID]),
    [dest] INT           NOT NULL CONSTRAINT FK_Rates_dest FOREIGN KEY REFERENCES [dbo].[Currencies] ([curID]),
    [day]  SMALLDATETIME NOT NULL,
    [val]  FLOAT (53)    NOT NULL,
	CONSTRAINT PK_Rates_src_dest_day PRIMARY KEY CLUSTERED ([src], [dest], [day])
);

INSERT INTO Currencies (code,name) VALUES ('EUR', 'Euro')
INSERT INTO Currencies (code,name) VALUES ('USD', 'American Dollar')
INSERT INTO Currencies (code,name) VALUES ('RUR', 'Russian Ruble')