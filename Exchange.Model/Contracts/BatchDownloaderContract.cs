﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.Model.Contracts
{
    [ContractClassFor(typeof(IBatchRatesDownloader))]
    abstract class BatchDownloaderContract : IBatchRatesDownloader
    {
        public RateRemote[] Download(ICurrency baseCurrency, DateTime[] dates)
        {
            Contract.Requires<ArgumentNullException>(baseCurrency != null);
            Contract.Requires<ArgumentNullException>(dates != null);
            return default(RateRemote[]);
        }
    }
}
