﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Exchange.Model.Contracts
{
    [ContractClassFor(typeof(ICurrencyRepository))]
    abstract class CurrencyRepositoryContract : ICurrencyRepository
    {
        public IEnumerable<ICurrency> GetAllCurrencies()
        {
            return default(IEnumerable<ICurrency>);
        }

        public ICurrency GetCurrency(string code)
        {
            Contract.Requires(code != null);
            return default(ICurrency);
        }
    }
}
