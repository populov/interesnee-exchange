﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Exchange.Model.Contracts
{
    [ContractClassFor(typeof(ICurrencyRepositoryWritable))]
    abstract class CurrencyRepositoryWritableContract : ICurrencyRepositoryWritable
    {
        public ICurrency InsertOrUpdate(string code, string name)
        {
            Contract.Requires(!string.IsNullOrEmpty(code));
            Contract.Requires(!string.IsNullOrEmpty(name));
            return default(ICurrency);
        }

        public abstract IEnumerable<ICurrency> GetAllCurrencies();
        public abstract ICurrency GetCurrency(string code);
    }
}
