﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model.OpenExchangeClient;
using Exchange.OpenExchangeClient;

namespace Exchange.Model.Contracts
{
    [ContractClassFor(typeof(IOpenExchangeClient))]
    abstract class OpenExchangeClientContract : IOpenExchangeClient
    {
        public DownloadKey GetDownloadKey(ICurrency baseCurrency, DateTime day)
        {
            Contract.Requires(baseCurrency != null);
            Contract.Requires(day != null);
            return default(DownloadKey);
        }

        public RateRemote[] GetRates(ICurrency baseCurrency, DateTime day)
        {
            Contract.Requires(baseCurrency != null);
            Contract.Requires(day != null);
            return default(RateRemote[]);
        }

        public CurrencyRemote[] GetCurrencies()
        {
            return default(CurrencyRemote[]);
        }
    }
}
