﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Exchange.Model.Contracts
{
    [ContractClassFor(typeof(IRatesRepository))]
    abstract internal class RatesRepositoryContract : IRatesRepository
    {
        public IEnumerable<IRate> GetRatesHistory(ICurrency @from, ICurrency to, DateTime since, DateTime upTo)
        {
            Contract.Requires<ArgumentNullException>(from != null);
            Contract.Requires<ArgumentNullException>(to != null);
            Contract.Requires<ArgumentNullException>(since != null);
            Contract.Requires<ArgumentNullException>(upTo != null);
            Contract.Requires<ArgumentException>(since <= upTo, "'Since' date can't be after 'upTo'");
            return default(IEnumerable<IRate>);
        }
    }
}
