﻿using System;
using System.Diagnostics.Contracts;

namespace Exchange.Model.Contracts
{
    [ContractClassFor(typeof(IRatesRepositoryWritable))]
    abstract class RatesRepositoryWritableContract : IRatesRepositoryWritable
    {
        public IRate InsertOrUpdate(ICurrency @from, ICurrency to, DateTime day, double val)
        {
            Contract.Requires<ArgumentNullException>(from != null);
            Contract.Requires<ArgumentNullException>(to != null);
            Contract.Requires<ArgumentNullException>(day != null);
            Contract.Requires<ArgumentOutOfRangeException>(val > 0);
            return default(IRate);
        }
    }
}
