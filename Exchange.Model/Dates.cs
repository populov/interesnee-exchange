﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Exchange.Model
{
    public static class Dates
    {
        private static readonly DateTime[] emptyDatesList = new DateTime[0];

        public static DateTime[] Range(DateTime since, DateTime upTo)
        {
            Contract.Requires<ArgumentNullException>(since != null);
            Contract.Requires<ArgumentNullException>(upTo != null);
            return GenerateMissingDatesList(since, upTo, emptyDatesList);
        }

        public static DateTime[] GenerateMissingDatesList(DateTime since, DateTime upTo, IEnumerable<IRate> rates)
        {
            return GenerateMissingDatesList(since, upTo, rates.Select(r => r.day).ToArray());
        }

        public static DateTime[] GenerateMissingDatesList(DateTime since, DateTime upTo, DateTime[] existingDates)
        {
            Contract.Requires<ArgumentNullException>(since != null);
            Contract.Requires<ArgumentNullException>(upTo != null);
            Contract.Requires<ArgumentNullException>(existingDates != null);

            var daysCount = upTo.Subtract(since).Days + 1;
            if (existingDates.Length == daysCount)
                return null;
            var result = new List<DateTime>();
            for (int i = 0; i < daysCount; i++)
            {
                var day = since.AddDays(i);
                if (!existingDates.Contains(day))
                    result.Add(day);
            }
            return result.ToArray();
        }

        public static string ToString(IEnumerable<DateTime> dates)
        {
            return dates == null ? "<none>" : string.Join(", ", dates.Select(d => d.ToShortDateString()));
        }
    }
}
