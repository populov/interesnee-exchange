﻿namespace Exchange.Model
{
    public interface ICurrency
    {
        int curID { get; }
        string code { get; }
        string name { get; }
    }
}
