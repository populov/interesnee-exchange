﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Exchange.Model.Contracts;

namespace Exchange.Model
{
    [ContractClass(typeof(CurrencyRepositoryContract))]
    public interface ICurrencyRepository
    {
        IEnumerable<ICurrency> GetAllCurrencies();
        ICurrency GetCurrency(string code);
    }
}
