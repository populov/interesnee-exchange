﻿using System.Diagnostics.Contracts;
using Exchange.Model.Contracts;

namespace Exchange.Model
{
    [ContractClass(typeof(CurrencyRepositoryWritableContract))]
    public interface ICurrencyRepositoryWritable : ICurrencyRepository
    {
        ICurrency InsertOrUpdate(string code, string name);
    }
}
