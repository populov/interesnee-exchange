﻿using System;

namespace Exchange.Model
{
    /// <summary>
    /// Currency exchange rate
    /// </summary>
    public interface IRate
    {
        int src { get; }
        int dest { get; }
        double val { get; }
        DateTime day { get; }
    }
}
