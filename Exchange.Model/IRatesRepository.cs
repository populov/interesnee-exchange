﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Exchange.Model.Contracts;

namespace Exchange.Model
{
    [ContractClass(typeof(RatesRepositoryContract))]
    public interface IRatesRepository
    {
        IEnumerable<IRate> GetRatesHistory(ICurrency from, ICurrency to, DateTime since, DateTime upTo);
    }
}
