﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model.Contracts;

namespace Exchange.Model
{
    [ContractClass(typeof(RatesRepositoryWritableContract))]
    public interface IRatesRepositoryWritable
    {
        IRate InsertOrUpdate(ICurrency from, ICurrency to, DateTime day, double val);
    }
}
