﻿namespace Exchange.Model.OpenExchangeClient
{
    public class CurrencyRemote
    {
        public CurrencyRemote(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; private set; }
        public string Name { get; private set; }
    }
}
