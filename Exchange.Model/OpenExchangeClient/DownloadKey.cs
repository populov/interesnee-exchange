﻿using System;

namespace Exchange.OpenExchangeClient
{
    public class DownloadKey : Tuple<DateTime, string> {
        public DownloadKey(DateTime date, string currencyCode) : base(date, currencyCode) {}
        public DateTime Date { get { return Item1; } }
        public string Code { get { return Item2; } }

        public override string ToString()
        {
            return string.Format("{0} per {1:d}", Code, Date);
        }
    }
}