﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model.Contracts;

namespace Exchange.Model.OpenExchangeClient
{
    [ContractClass(typeof(BatchDownloaderContract))]
    public interface IBatchRatesDownloader
    {
        RateRemote[] Download(ICurrency baseCurrency, DateTime[] dates);
    }
}
