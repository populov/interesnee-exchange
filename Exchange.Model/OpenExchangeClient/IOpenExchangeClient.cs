﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model.Contracts;
using Exchange.OpenExchangeClient;

namespace Exchange.Model.OpenExchangeClient
{
    [ContractClass(typeof(OpenExchangeClientContract))]
    public interface IOpenExchangeClient
    {
        DownloadKey GetDownloadKey(ICurrency baseCurrency, DateTime day);
        RateRemote[] GetRates(ICurrency baseCurrency, DateTime day);
        CurrencyRemote[] GetCurrencies();
    }
}
