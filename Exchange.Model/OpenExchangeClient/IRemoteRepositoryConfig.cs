﻿namespace Exchange.Model.OpenExchangeClient
{
    public interface IRemoteRepositoryConfig
    {
        string ApiUrl { get; }
        string AppId { get; }
        int TimeoutMilliseconds { get; }
    }
}