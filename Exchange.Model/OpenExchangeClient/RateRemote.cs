﻿using System;

namespace Exchange.Model.OpenExchangeClient
{
    public class RateRemote
    {
        public RateRemote(string src, string dest, DateTime day, double val)
        {
            Src = src;
            Dest = dest;
            Day = day;
            Val = val;
        }

        public string Src { get; private set; }
        public string Dest { get; private set; }
        public DateTime Day { get; set; }
        public double Val { get; private set; }

        public override string ToString()
        {
            return string.Format("{2:d} {0}->{1} = {3}", Src, Dest, Day, Val);
        }
    }
}
