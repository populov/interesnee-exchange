﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.Syncronization;

namespace Exchange.Model
{
    public static class Rates
    {
        public static IEnumerable<IRate> Merge(IEnumerable<IRate> priorityList, IEnumerable<IRate> secondaryList)
        {
            if (secondaryList == null || !secondaryList.Any())
                return priorityList;

            if (priorityList == null || !priorityList.Any())
                return secondaryList;

            var missingInPrimary = secondaryList.Where(storedRate =>!priorityList.Any(r => r.src == storedRate.src && r.dest == storedRate.dest && r.day == storedRate.day));
            var result = priorityList.Union(missingInPrimary);
            return result;
        }

        public static double[] GetArraingedArray(IEnumerable<IRate> rates, ICurrency from, ICurrency to, DateTime since, DateTime upTo)
        {
            Contract.Requires<ArgumentNullException>(rates != null);
            Contract.Requires<ArgumentNullException>(from != null);
            Contract.Requires<ArgumentNullException>(to != null);
            Contract.Requires<ArgumentNullException>(since != null);
            Contract.Requires<ArgumentNullException>(upTo != null);

            return ZeroMissingDates(rates, from, to, since, upTo)
                .OrderBy(r => r.day)
                .Select(r => r.val > 0.01 ? Math.Round(r.val, 5) : r.val)
                .ToArray();
        }

        private static IEnumerable<IRate> ZeroMissingDates(IEnumerable<IRate> rates, ICurrency from, ICurrency to, DateTime since, DateTime upTo)
        {
            var missingDates = Dates.GenerateMissingDatesList(since, upTo, rates.Select(r => r.day).ToArray());
            if (missingDates != null && missingDates.Any())
                rates = rates.Union(missingDates.Select(day => new RatePOCO(from.curID, to.curID, day, 0)));

            return rates;
        }
    }
}
