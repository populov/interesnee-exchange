﻿using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.Model.Syncronization
{
    public class CurrencyUpdater
    {
        private readonly ICurrencyRepositoryWritable currencyRepository;
        private readonly IOpenExchangeClient openExchangeClient;
        private static bool syncComplete;

        public CurrencyUpdater(ICurrencyRepositoryWritable currencyRepository, IOpenExchangeClient openExchangeClient)
        {
            this.currencyRepository = currencyRepository;
            this.openExchangeClient = openExchangeClient;
        }

        public void Sync()
        {
            if (syncComplete)
                return;
            var currencyRemotes = openExchangeClient.GetCurrencies();
            var currencySaved = currencyRepository.GetAllCurrencies().ToArray();
            var newCurrencies = currencyRemotes.Where(cNew =>
                currencySaved.All(cSaved => cSaved.code != cNew.Code && cSaved.name != cNew.Name));
            foreach (var currency in newCurrencies)
                currencyRepository.InsertOrUpdate(currency.Code, currency.Name);
            syncComplete = true;
        }

        public ICurrency GetCurrency(string code)
        {
            Contract.Requires(!string.IsNullOrEmpty(code));
            return currencyRepository.GetCurrency(code);
        }
    }
}
