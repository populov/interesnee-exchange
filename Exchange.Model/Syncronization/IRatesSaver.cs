﻿using System.Collections.Generic;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.Model.Syncronization
{
    public interface IRatesSaver
    {
        void SaveRatesInBackground(IEnumerable<RateRemote> rates);
    }
}
