﻿using System;

namespace Exchange.Model.Syncronization
{
    public interface IRatesSavingQueue : IRatesSaver
    {
        double GetFromQueue(string src, string dest, DateTime day);
    }
}
