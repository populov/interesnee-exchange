using System;

namespace Exchange.Model.Syncronization
{
    internal class RatePOCO : IRate
    {
        public RatePOCO(int src, int dest, DateTime day, double val)
        {
            this.src = src;
            this.dest = dest;
            this.val = val;
            this.day = day;
        }

        public int src { get; private set; }
        public int dest { get; private set; }
        public double val { get; private set; }
        public DateTime day { get; private set; }

        public override string ToString()
        {
            return String.Format("src={0}, dest={1}, val={2}, day={3:d}", src, dest, val, day);
        }
    }
}