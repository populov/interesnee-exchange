﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.Model.Syncronization
{
    public class RatesRepositoryBackgroundSaving : IRatesSaver, IRatesRepository
    {
        private readonly IRatesRepository ratesRepository;
        private readonly IRatesSavingQueue ratesSavingQueue;

        public RatesRepositoryBackgroundSaving(IRatesRepository ratesRepository, IRatesSavingQueue ratesSavingQueue)
        {
            Contract.Requires<ArgumentNullException>(ratesRepository != null);
            Contract.Requires<ArgumentNullException>(ratesSavingQueue != null);
            this.ratesRepository = ratesRepository;
            this.ratesSavingQueue = ratesSavingQueue;
        }

        public void SaveRatesInBackground(IEnumerable<RateRemote> rates)
        {
            ratesSavingQueue.SaveRatesInBackground(rates);
        }

        public IEnumerable<IRate> GetRatesHistory(ICurrency from, ICurrency to, DateTime since, DateTime upTo)
        {
            var pendingSaveRates = (from date in Dates.Range(since, upTo)
                let pendingSaveRate = ratesSavingQueue.GetFromQueue(@from.code, to.code, date)
                where pendingSaveRate > 0
                select new RatePOCO(@from.curID, to.curID, date, pendingSaveRate));
            var storedRates = ratesRepository.GetRatesHistory(from, to, since, upTo);
            return Rates.Merge(pendingSaveRates, storedRates);
        }
    }
}
