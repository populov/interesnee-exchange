﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.Model.Syncronization
{
    public class RatesRepositoryComposite : IRatesRepository
    {
        private readonly IRatesRepository ratesRepository;
        private readonly IBatchRatesDownloader batchRatesDownloader;
        private readonly IRatesSaver ratesSaver;

        public RatesRepositoryComposite(IRatesRepository ratesRepository, IBatchRatesDownloader batchRatesDownloader, IRatesSaver ratesSaver)
        {
            Contract.Requires<ArgumentNullException>(ratesRepository != null);
            Contract.Requires<ArgumentNullException>(batchRatesDownloader != null);
            Contract.Requires<ArgumentNullException>(ratesSaver != null);

            this.ratesRepository = ratesRepository;
            this.batchRatesDownloader = batchRatesDownloader;
            this.ratesSaver = ratesSaver;
        }

        public IEnumerable<IRate> GetRatesHistory(ICurrency from, ICurrency to, DateTime since, DateTime upTo)
        {
            var rates = ratesRepository.GetRatesHistory(from, to, since, upTo);
            var storedRates = rates as IRate[] ?? rates.ToArray();
            var missingDates = Dates.GenerateMissingDatesList(since, upTo, storedRates);
            if (missingDates == null || !missingDates.Any())
                return storedRates;

            ICollection<RateRemote> downloadedRates = batchRatesDownloader.Download(from, missingDates);
            ratesSaver.SaveRatesInBackground(downloadedRates);
            downloadedRates = GetOrConvert(downloadedRates, from.code, to.code, missingDates);
            var result = Merge(storedRates, downloadedRates, @from, to);
            return result;
        }

        private static IEnumerable<IRate> Merge(IEnumerable<IRate> storedRates, IEnumerable<RateRemote> allDownloadedRates, ICurrency from, ICurrency to)
        {
            var newRates = allDownloadedRates.Select(r => new RatePOCO(@from.curID, to.curID, r.Day, r.Val));
            return Rates.Merge(newRates, storedRates);
        }

        public static IList<RateRemote> GetOrConvert(IEnumerable<RateRemote> rates, string src, string dest, DateTime[] days)
        {
            Contract.Requires<ArgumentNullException>(rates != null);
            Contract.Requires<ArgumentNullException>(src != null);
            Contract.Requires<ArgumentNullException>(dest != null);

            var result = new List<RateRemote>(days.Length);
            var ratesArray = rates as RateRemote[] ?? rates.ToArray();
            // exact match
            result.AddRange(ratesArray.Where(r => r.Src == src && r.Dest == dest));
            if (result.Count == days.Length)
                return result;
            // convert missing
            var converted = (from rFrom in (ratesArray.Where(r => r.Dest == src))
                             join rTo in (ratesArray.Where(r => r.Dest == dest))
                                 on new { @base = rFrom.Src, rFrom.Day } equals new { @base = rTo.Src, rTo.Day }
                             select new RateRemote(rFrom.Dest, rTo.Dest, rFrom.Day, rFrom.Val / rTo.Val));

            result.AddRange(converted.Distinct());
            return result;
        }
    }
}
