﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.OpenExchangeClient;
using log4net;

namespace Exchange.Model.Syncronization
{
    public class RatesRepositoryConverting : IRatesRepository
    {
        private readonly IRatesRepository ratesRepository;
        private readonly IRatesSaver ratesSaver;
        private readonly ILog log;
        private readonly ICurrency usd;

        public RatesRepositoryConverting(IRatesRepository ratesRepository, IRatesSaver ratesSaver, ICurrencyRepository currencyRepository, ILog log)
        {
            Contract.Requires<ArgumentNullException>(ratesRepository != null);
            Contract.Requires<ArgumentNullException>(ratesSaver != null);
            Contract.Requires<ArgumentNullException>(currencyRepository != null);
            this.ratesRepository = ratesRepository;
            this.ratesSaver = ratesSaver;
            this.log = log;
            usd = currencyRepository.GetCurrency("USD");
        }

        public IEnumerable<IRate> GetRatesHistory(ICurrency from, ICurrency to, DateTime since, DateTime upTo)
        {
            var savedEnum = ratesRepository.GetRatesHistory(from, to, since, upTo);
            var saved = savedEnum as IRate[] ?? savedEnum.ToArray();
            var missingDates = Dates.GenerateMissingDatesList(since, upTo, saved);
            if (missingDates == null || !missingDates.Any())
                return saved;
            var converted = GetConverted(from, to, since, upTo, missingDates).ToArray();
            if (converted.Length == 0)
                return saved;
            log.Info(string.Format("Converted {0}->{1}: {2}", from.code, to.code, Dates.ToString(converted.Select(r => r.day))));
            SaveInBackground(converted, from, to);
            return Rates.Merge(saved, converted);
        }

        private IEnumerable<IRate> GetConverted(ICurrency from, ICurrency to, DateTime since, DateTime upTo, IEnumerable<DateTime> missingDates)
        {
            var usdBasedFrom = ratesRepository.GetRatesHistory(usd, from, since, upTo);
            var usdBasedTo = ratesRepository.GetRatesHistory(usd, to, since, upTo);
            return from date in missingDates
                let rFrom = usdBasedFrom.FirstOrDefault(r => r.day == date) where rFrom != null
                let rTo = usdBasedTo.FirstOrDefault(r => r.day == date) where rTo != null
                   select new RatePOCO(rFrom.dest, rTo.dest, rFrom.day, rTo.val / rFrom.val);
        }

        private void SaveInBackground(IEnumerable<IRate> convertedRates, ICurrency from, ICurrency to)
        {
            var rates = from r in convertedRates select new RateRemote(@from.code, to.code, r.day, r.val);
            ratesSaver.SaveRatesInBackground(rates);
        }
    }
}
