﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using Exchange.Model.OpenExchangeClient;
using log4net;

namespace Exchange.Model.Syncronization
{
    public class RatesSaverSingleThread : IRatesSavingQueue
    {
        private static readonly object syncRoot = new object();
        private static readonly Dictionary<QueueKey, double> queue = new Dictionary<QueueKey, double>();
        private static BackgroundSaver runningSaver;

        private readonly IRatesRepositoryWritable ratesRepository;
        private readonly ICurrencyRepository currencyRepository;
        private readonly CurrencyUpdater currencyUpdater;
        private readonly ILog log;
        private readonly List<string> obsoleteCurrencies = new List<string>();

        public RatesSaverSingleThread(IRatesRepositoryWritable ratesRepository, ICurrencyRepository currencyRepository, CurrencyUpdater currencyUpdater, ILog log)
        {
            Contract.Requires<ArgumentNullException>(ratesRepository != null);
            Contract.Requires<ArgumentNullException>(currencyRepository != null);
            Contract.Requires<ArgumentNullException>(currencyUpdater != null);
            Contract.Requires<ArgumentNullException>(log != null);

            this.ratesRepository = ratesRepository;
            this.currencyRepository = currencyRepository;
            this.currencyUpdater = currencyUpdater;
            this.log = log;
        }

        public void SaveRatesInBackground(IEnumerable<RateRemote> rates)
        {
            if (rates == null)
                return;
            AddToQueue(rates as RateRemote[] ?? rates.ToArray());
            lock (syncRoot)
            {
                if (runningSaver != null)
                    return;
                runningSaver = new BackgroundSaver(log);
            }
            runningSaver.SaveRate += SaveRate;
            runningSaver.OnQueueEmpty += RemoveSaverReference;
            runningSaver.Start();
        }

        private void RemoveSaverReference(BackgroundSaver obj)
        {
            lock (syncRoot)
            {
                runningSaver = null;
            }
        }

        private void AddToQueue(ICollection<RateRemote> rates)
        {
            if (rates.Count == 0)
                return;
            log.Info("Saving " + rates.Count + " new rates");
            foreach (var rate in rates)
            {
                var key = new QueueKey(rate.Src, rate.Dest, rate.Day);
                lock (queue)
                {
                    if (queue.ContainsKey(key))
                        continue;
                    queue.Add(key, rate.Val);
                }
            }
        }

        public double GetFromQueue(string src, string dest, DateTime day)
        {
            double result;
            return queue.TryGetValue(new QueueKey(src, dest, day), out result) ? result : 0;
        }

        private void SaveRate(string src, string dest, DateTime day, double val)
        {
            var from = TryGetCurrencyOrSync(src);
            if (from == null)
            {
                log.Warn(string.Format("Cannot find currency {0}, skip saving rate {1}", src, new RateRemote(src, dest, day, val)));
                return;
            }
            var to = TryGetCurrencyOrSync(dest);
            if (to == null)
            {
                log.Warn(string.Format("Cannot find currency {0}, skip saving rate {1}", dest, new RateRemote(src, dest, day, val)));
                return;
            }
            ratesRepository.InsertOrUpdate(from, to, day, val);
        }

        private ICurrency TryGetCurrencyOrSync(string code)
        {
            var result = currencyRepository.GetCurrency(code);
            if (result != null || obsoleteCurrencies.Contains(code))
                return result;

            currencyUpdater.Sync();
            result = currencyUpdater.GetCurrency(code);
            if (result == null)
                obsoleteCurrencies.Add(code);
            return result;
        }

        private class QueueKey : Tuple<string, string, DateTime> {
            public QueueKey(string src, string dest, DateTime day) : base(src, dest, day) {}

            public string Src { get { return Item1; } }
            public string Dest { get { return Item2; } }
            public DateTime Day { get { return Item3; } }
        }

        private class BackgroundSaver
        {
            private readonly ILog log;
            public Action<BackgroundSaver> OnQueueEmpty;
            public Action<string, string, DateTime, double> SaveRate;

            public BackgroundSaver(ILog log)
            {
                this.log = log;
            }

            public void Start()
            {
                new Thread(ProcessRatesQueue).Start();
            }

            private void ProcessRatesQueue()
            {
                var timestamp = DateTime.Now;
                var queueHasElements = true;
                while (queueHasElements)
                    queueHasElements = ProcessQueueStep();
                log.Debug("Saving rates job complete, time taken: " + DateTime.Now.Subtract(timestamp));
                if (OnQueueEmpty != null)
                    OnQueueEmpty(this);
            }

            private bool ProcessQueueStep()
            {
                QueueKey key;
                double value;
                int itemsLeft;
                lock (queue)
                {
                    key = queue.Keys.FirstOrDefault();
                    if (key == null)
                        return false;
                    itemsLeft = queue.Count;
                    value = queue[key];
                }
                if (log.IsDebugEnabled && itemsLeft % 500 == 0)
                    log.Debug(string.Format("Background rates saver: {0} items left", itemsLeft));
                SaveRateSafe(key.Src, key.Dest, key.Day, value);
                bool result;
                lock (queue)
                {
                    queue.Remove(key);
                    result = queue.Count > 0;
                }
                return result;
            }

            private void SaveRateSafe(string src, string dest, DateTime day, double val)
            {
                try
                {
                    if (SaveRate != null)
                        SaveRate(src, dest, day, val);
                }
                catch(Exception e)
                {
                    log.Error(string.Format("Error saving rate {0}", new RateRemote(src, dest, day, val)), e);
                }
            }
        }
    }
}
