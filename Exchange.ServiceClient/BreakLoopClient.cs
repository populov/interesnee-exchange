﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.OpenExchangeClient
{
    public class BreakLoopClient : IOpenExchangeClient
    {
        private const int breakLoopDelayMilliseconds = 1000;
        private readonly IOpenExchangeClient openExchangeClient;
        private DateTime? lastErrorTime;

        public BreakLoopClient(IOpenExchangeClient openExchangeClient)
        {
            Contract.Requires<ArgumentNullException>(openExchangeClient != null);
            this.openExchangeClient = openExchangeClient;
        }

        public DownloadKey GetDownloadKey(ICurrency baseCurrency, DateTime day)
        {
            return openExchangeClient.GetDownloadKey(baseCurrency, day);
        }

        public RateRemote[] GetRates(ICurrency @from, DateTime day)
        {
            return openExchangeClient.GetRates(from, day);
        }

        public CurrencyRemote[] GetCurrencies()
        {
            if (lastErrorTime == null)
                return DetectErrorAndReturn();
            var elapsedSinceLastError = DateTime.Now.Subtract(lastErrorTime.Value);
            if (elapsedSinceLastError.Milliseconds < breakLoopDelayMilliseconds)
                return new CurrencyRemote[0];
            lastErrorTime = null;
            return DetectErrorAndReturn();
        }

        private CurrencyRemote[] DetectErrorAndReturn()
        {
            var currencies = openExchangeClient.GetCurrencies();
            if (currencies == null || currencies.Length == 0)
                lastErrorTime = DateTime.Now;
            return currencies;
        }
    }
}
