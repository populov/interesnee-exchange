﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.OpenExchangeClient;
using log4net;
using Newtonsoft.Json;

namespace Exchange.OpenExchangeClient
{
    public class CurrencyResponseParser
    {
        private readonly ILog log;

        public CurrencyResponseParser(ILog log)
        {
            Contract.Requires<ArgumentNullException>(log != null);
            this.log = log;
        }

        private CurrencyRemote[] ParseUnsafe(string content)
        {
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
            return dictionary.Select(item => new CurrencyRemote(item.Key, item.Value)).ToArray();
        }

        public CurrencyRemote[] Parse(string content)
        {
            Contract.Requires(content != null);
            try
            {
                return ParseUnsafe(content);
            }
            catch (Exception e)
            {
                log.Error("Can't parse currency response", e);
                return new CurrencyRemote[0];
            }
        }
    }
}
