﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using log4net;

namespace Exchange.OpenExchangeClient
{
    public class MultithreadRatesDownloader : IBatchRatesDownloader
    {
        private static readonly object syncRoot = new object();
        private static readonly Dictionary<DownloadKey, BackgroundDownloader> downloadJobs = new Dictionary<DownloadKey, BackgroundDownloader>();

        private readonly int timeoutMilliseconds;
        private readonly IOpenExchangeClient openExchangeClient;
        private readonly ILog log;

        public MultithreadRatesDownloader(IOpenExchangeClient openExchangeClient, IRemoteRepositoryConfig config, ILog log)
        {
            Contract.Requires<ArgumentNullException>(openExchangeClient != null);
            Contract.Requires<ArgumentNullException>(config != null);
            Contract.Requires<ArgumentNullException>(log != null);

            this.openExchangeClient = openExchangeClient;
            this.log = log;
            timeoutMilliseconds = config.TimeoutMilliseconds;
        }

        public RateRemote[] Download(ICurrency baseCurrency, DateTime[] dates)
        {
            log.Info(string.Format("Downloading {1}-based rates {0}", Dates.ToString(dates),
                openExchangeClient.GetDownloadKey(baseCurrency, DateTime.Today).Code));
            var workers = dates.Select(date => GetOrCreateDownloadJob(date, baseCurrency)).ToArray();
            bool workIncomplete = true;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (workIncomplete && stopwatch.ElapsedMilliseconds < timeoutMilliseconds)
            {
                Thread.Sleep(300);
                workIncomplete = workers.Any(worker => !worker.DownloadComplete);
            }
            stopwatch.Stop();
            if (workIncomplete)
                foreach (var worker in workers.Where(worker => !worker.DownloadComplete)) {
                    log.Warn("Downloading rates " + worker.Key + " timed out, skipping");
                }
            if (log.IsDebugEnabled)
                log.Debug(string.Format("Download complete in {0} ms ({1})", stopwatch.ElapsedMilliseconds, Dates.ToString(dates)));
            return workers.Where(w => w.DownloadComplete).SelectMany(w => w.Result).ToArray();
        }

        private BackgroundDownloader GetOrCreateDownloadJob(DateTime date, ICurrency currency)
        {
            var key = openExchangeClient.GetDownloadKey(currency, date);
            BackgroundDownloader worker;
            lock (syncRoot)
            {
                if (downloadJobs.TryGetValue(key, out worker))
                    return worker;
                // We assume, that openExchangeClient is threadsafe, otherwise, we shoud provide abstract factory of IOpenExchangeClient
                worker = new BackgroundDownloader(openExchangeClient, date, currency, log);
                downloadJobs.Add(key, worker);
            }
            worker.OnDownloadComplete += RemoveDownloadJob;
            worker.StartDownload();
            return worker;
        }

        private static void RemoveDownloadJob(BackgroundDownloader worker)
        {
            lock (syncRoot)
            {
                downloadJobs.Remove(worker.Key);
            }
        }

        private class BackgroundDownloader
        {
            private readonly IOpenExchangeClient openExchangeClient;
            private readonly ICurrency currency;
            private readonly ILog log;
            public readonly DownloadKey Key;

            public Action<BackgroundDownloader> OnDownloadComplete; 

            public BackgroundDownloader(IOpenExchangeClient openExchangeClient, DateTime date, ICurrency currency, ILog log)
            {
                this.openExchangeClient = openExchangeClient;
                this.currency = currency;
                this.log = log;
                Key = openExchangeClient.GetDownloadKey(currency, date);
            }

            public void StartDownload()
            {
                new Thread(Download).Start();
            }

            private void Download()
            {
                try
                {
                    Result = openExchangeClient.GetRates(currency, Key.Date);
                }
                catch (Exception e)
                {
                    Result = new RateRemote[0];
                    log.Error("Error downloading rates " + Key, e);
                }
                if (OnDownloadComplete != null)
                    OnDownloadComplete(this);
            }

            public IEnumerable<RateRemote> Result { get; private set; }

            public bool DownloadComplete { get { return Result != null; } }
        }
    }
}
