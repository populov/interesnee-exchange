﻿using System;
using System.Diagnostics.Contracts;
using System.Net;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using log4net;

namespace Exchange.OpenExchangeClient
{
    public abstract class OpenExchangeClientBase : IOpenExchangeClient
    {
        private readonly IRemoteRepositoryConfig config;
        private readonly RatesResponseParser ratesResponseParser;
        private readonly CurrencyResponseParser currencyResponseParser;
        private readonly Uri currenciesUri;
        private readonly Uri baseApiUri;
        private readonly ILog log;

        protected OpenExchangeClientBase(IRemoteRepositoryConfig config, RatesResponseParser ratesResponseParser, CurrencyResponseParser currencyResponseParser, ILog log)
        {
            Contract.Requires<ArgumentNullException>(config != null);
            Contract.Requires<ArgumentNullException>(ratesResponseParser != null);
            Contract.Requires<ArgumentNullException>(currencyResponseParser != null);
            Contract.Requires<ArgumentNullException>(log != null);

            this.config = config;
            this.ratesResponseParser = ratesResponseParser;
            this.currencyResponseParser = currencyResponseParser;
            this.log = log;
            baseApiUri = new Uri(config.ApiUrl);
            currenciesUri = new Uri(baseApiUri, string.Format("currencies.json?app_id={0}", config.AppId));
        }

        public DownloadKey GetDownloadKey(ICurrency baseCurrency, DateTime day)
        {
            return new DownloadKey(day, GetActualCurrencyCode(baseCurrency));
        }

        protected abstract string GetActualCurrencyCode(ICurrency baseCurrency);

        public virtual RateRemote[] GetRates(ICurrency baseCurrency, DateTime day)
        {
            string responseText;
            try
            {
                var historyUrl = GetRatesUrl(baseCurrency, day);
                responseText = new WebDownload(config.TimeoutMilliseconds).DownloadString(historyUrl);
                log.Info(string.Format("Downloaded rates per {0:d}", day));
            }
            catch (Exception e)
            {
                log.Error(string.Format("Can't download rates per {0:d}", day), e);
                return new RateRemote[0];
            }
            return ratesResponseParser.Parse(responseText, day);
        }

        public CurrencyRemote[] GetCurrencies()
        {
            String responseText;
            try
            {
                responseText = new WebClient().DownloadString(currenciesUri);
                log.Info("Downloaded currencies list");
            }
            catch (Exception e)
            {
                log.Error("Can't download currency list", e);
                return new CurrencyRemote[0];
            }
            return currencyResponseParser.Parse(responseText);
        }

        protected virtual Uri GetRatesUrl(ICurrency baseCurrency, DateTime day)
        {
            if (day == DateTime.Today)
                return new Uri(baseApiUri, string.Format("latest.json?app_id={0}", config.AppId));
            return new Uri(baseApiUri, string.Format("historical/{0:yyy-MM-dd}.json?app_id={1}", day, config.AppId));
        }

        private class WebDownload : WebClient
        {
            private readonly int timeout;

            public WebDownload(int timeout)
            {
                this.timeout = timeout;
            }

            protected override WebRequest GetWebRequest(Uri address)
            {
                var request = base.GetWebRequest(address);
                if (request != null)
                {
                    request.Timeout = timeout;
                }
                return request;
            }
        }
    }
}
