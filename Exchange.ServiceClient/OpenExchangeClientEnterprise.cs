﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using log4net;

namespace Exchange.OpenExchangeClient
{
    /// <summary>
    /// Payed clients can get rates based on any currency
    /// </summary>
    public class OpenExchangeClientEnterprise : OpenExchangeClientBase
    {
        public OpenExchangeClientEnterprise(IRemoteRepositoryConfig config, RatesResponseParser ratesResponseParser,
                                            CurrencyResponseParser currencyResponseParser, ILog log)
            : base(config, ratesResponseParser, currencyResponseParser, log)
        {
            Contract.Requires<ArgumentNullException>(config != null);
            Contract.Requires<ArgumentNullException>(ratesResponseParser != null);
            Contract.Requires<ArgumentNullException>(currencyResponseParser != null);
            Contract.Requires<ArgumentNullException>(log != null);
        }

        protected override string GetActualCurrencyCode(ICurrency baseCurrency)
        {
            return baseCurrency.code;
        }

        protected override Uri GetRatesUrl(ICurrency baseCurrency, DateTime day)
        {
            return new Uri(base.GetRatesUrl(baseCurrency, day) + "&base=" + baseCurrency.code);
        }
    }
}
