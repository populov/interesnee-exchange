﻿using System;
using System.Diagnostics.Contracts;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using log4net;

namespace Exchange.OpenExchangeClient
{
    /// <summary>
    /// Free clients may get only USD-based exchange rates.
    /// </summary>
    public class OpenExchangeClientFree : OpenExchangeClientBase
    {
        public OpenExchangeClientFree(IRemoteRepositoryConfig config, RatesResponseParser ratesResponseParser,
                                      CurrencyResponseParser currencyResponseParser, ILog log)
            : base(config, ratesResponseParser, currencyResponseParser, log)
        {
            Contract.Requires<ArgumentNullException>(config != null);
            Contract.Requires<ArgumentNullException>(ratesResponseParser != null);
            Contract.Requires<ArgumentNullException>(currencyResponseParser != null);
            Contract.Requires<ArgumentNullException>(log != null);
        }

        protected override string GetActualCurrencyCode(ICurrency baseCurrency)
        {
            return "USD";
        }
    }
}
