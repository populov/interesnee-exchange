﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model.OpenExchangeClient;
using log4net;
using Newtonsoft.Json;

namespace Exchange.OpenExchangeClient
{
    public class RatesResponseParser
    {
        private readonly ILog log;

        public RatesResponseParser(ILog log)
        {
            Contract.Requires(log != null);
            this.log = log;
        }

        private RateRemote[] ParseUnsafe(string content, DateTime day)
        {
            var response = JsonConvert.DeserializeObject<RatesResponse>(content);
            return (from rate in response.rates
                    select new RateRemote(response.BaseCurrency, rate.Key, day, rate.Value)).ToArray();
        }

        public RateRemote[] Parse(string content, DateTime day)
        {
            Contract.Requires(content != null);
            Contract.Requires(day != null);
            try
            {
                return ParseUnsafe(content, day);
            }
            catch (Exception e)
            {
                log.Error("Can't parse rates response", e);
                return new RateRemote[0];
            }
        }

        private class RatesResponse
        {
            [JsonProperty("disclaimer")]
            public string Disclaimer { get; set; }

            [JsonProperty("license")]
            public string License { get; set; }

            [JsonProperty("timestamp")]
            public long Timestamp { get; set; }

            [JsonProperty("base")]
            public string BaseCurrency { get; set; }

            [JsonProperty("rates")]
            public IDictionary<string, float> rates { get; set; }
        }
    }
}
