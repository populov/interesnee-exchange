﻿using System.Configuration;
using Exchange.Model.OpenExchangeClient;

namespace Exchange.OpenExchangeClient
{
    public class RemoteRepositoryConfig : IRemoteRepositoryConfig
    {
        public RemoteRepositoryConfig()
        {
            var config = (RemoteRepositoryConfigurationSection) ConfigurationManager.GetSection("exchange.remote");
            ApiUrl = config.GetHistoryUrl;
            AppId = config.AppId;
            TimeoutMilliseconds = config.TimeoutMilliseconds;
        }

        public string ApiUrl { get; private set; }
        public string AppId { get; private set; }
        public int TimeoutMilliseconds { get; private set; }
    }
}
