﻿using System.Configuration;

namespace Exchange.OpenExchangeClient
{
    public class RemoteRepositoryConfigurationSection : ConfigurationSection
    {
        private const string timeoutMilliseconds = "timeoutMilliseconds";
        private const string appId = "appId";
        private const string baseApiUrl = "baseApiUrl";

        [ConfigurationProperty(baseApiUrl, IsRequired = false, DefaultValue = "http://openexchangerates.org/api/")]
        public string GetHistoryUrl
        {
            get { return (string)this[baseApiUrl]; }
        }

        [ConfigurationProperty(appId, IsRequired = true)]
        public string AppId {
            get { return (string) this[appId]; }
        }

        [ConfigurationProperty(timeoutMilliseconds, IsRequired = false, DefaultValue = 3000)]
        public int TimeoutMilliseconds { get { return (int) this[timeoutMilliseconds]; } }
    }
}
