﻿using System.Configuration;
using Exchange.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exchange.Tests.Config
{
    [TestClass]
    public class ExchangeUIConfig
    {
        [TestMethod]
        public void ReadConfigFile()
        {
            var section = (ExchangeUIConfigurationSection)ConfigurationManager.GetSection("exchange.web.ui");
            Assert.AreEqual(100, section.MaxRangeDays);
            Assert.AreEqual("dd.mm.yyyy", section.DateFormat);
        }
    }
}
