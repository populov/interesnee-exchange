﻿using System;
using Exchange.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exchange.Tests.Controllers
{
    [TestClass]
    public class RatesControllerTest
    {
        [TestMethod]
        public void ParseDate_OK()
        {
            var actual = RatesController.ParseDate("2003-02-01");
            Assert.AreEqual(new DateTime(2003, 2, 1), actual);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void ParseDate_Fail1()
        {
            var date = RatesController.ParseDate("01.02.03");
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void ParseDate_Fail2()
        {
            var date = RatesController.ParseDate("01-02-2003");
        }
    }
}
