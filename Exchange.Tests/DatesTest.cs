﻿using System;
using System.Linq;
using Exchange.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exchange.Tests
{
    [TestClass]
    public class DatesTest
    {
        private readonly DateTime today = DateTime.Today;
        private readonly DateTime yesterday = DateTime.Today.AddDays(-1);
        private readonly DateTime beforeYesterday = DateTime.Today.AddDays(-2);

        [TestMethod]
        public void Range()
        {
            var result = Dates.Range(beforeYesterday, today);
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual(beforeYesterday, result[0]);
            Assert.AreEqual(yesterday, result[1]);
            Assert.AreEqual(today, result[2]);
        }

        [TestMethod]
        public void GenerateMissingDatesList_None()
        {
            var missingDates = Dates.GenerateMissingDatesList(today, today, new[] { today });
            Assert.IsTrue(missingDates == null || !missingDates.Any());
        }

        [TestMethod]
        public void GenerateMissingDatesList_1()
        {
            var missingDates = Dates.GenerateMissingDatesList(today, today, new DateTime[] { });
            Assert.IsNotNull(missingDates);
            Assert.AreEqual(1, missingDates.Count());
            Assert.AreEqual(DateTime.Today, missingDates.First());
        }

        [TestMethod]
        public void GenerateMissingDatesList_2()
        {
            var existingDates = new[] { yesterday };
            var missingDates = Dates.GenerateMissingDatesList(beforeYesterday, today, existingDates);
            Assert.IsNotNull(missingDates);
            Assert.AreEqual(2, missingDates.Count());
            Assert.AreEqual(beforeYesterday, missingDates[0]);
            Assert.AreEqual(today, missingDates[1]);
        }

        [TestMethod]
        public void ListToString()
        {
            var result = Dates.ToString(new[] {today, yesterday});
            Assert.AreEqual(string.Format("{0:d}, {1:d}", today,yesterday), result);
        }
    }
}
