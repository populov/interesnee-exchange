﻿using System.Linq;
using System.Reflection;
using Exchange.Controllers;
using Exchange.Model;
using Exchange.OpenExchangeClient;
using Exchange.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exchange.Tests
{
    [TestClass]
    public class Dependencies
    {
        private readonly Assembly modelLibrary = typeof(ICurrency).Assembly;
        private readonly Assembly storageLibrary = typeof(ExchangeDBDataContext).Assembly;
        private readonly Assembly webApp = typeof(HomeController).Assembly;
        private readonly Assembly openExchangeClient = typeof(OpenExchangeClientFree).Assembly;
        private readonly Assembly testsLibrary = typeof(Dependencies).Assembly;

        [TestMethod]
        public void ModelDoesNotDependOnImplementation()
        {
            Assert.IsFalse(modelLibrary.DependsOn(storageLibrary));
            Assert.IsFalse(modelLibrary.DependsOn(webApp));
            Assert.IsFalse(modelLibrary.DependsOn(openExchangeClient));
        }

        [TestMethod]
        public void ProductionDoesNotDependOnTests()
        {
            Assert.IsFalse(modelLibrary.DependsOn(testsLibrary));
            Assert.IsFalse(storageLibrary.DependsOn(testsLibrary));
            Assert.IsFalse(webApp.DependsOn(testsLibrary));
            Assert.IsFalse(openExchangeClient.DependsOn(testsLibrary));
        }

        [TestMethod]
        public void ServiceClientDependsOnModelOnly()
        {
            Assert.IsTrue(openExchangeClient.DependsOn(modelLibrary));
            Assert.IsFalse(openExchangeClient.DependsOn(storageLibrary));
            Assert.IsFalse(openExchangeClient.DependsOn(webApp));
        }

        [TestMethod]
        public void StorateDependsOnModelOnly()
        {
            Assert.IsTrue(storageLibrary.DependsOn(modelLibrary));
            Assert.IsFalse(storageLibrary.DependsOn(openExchangeClient));
            Assert.IsFalse(storageLibrary.DependsOn(webApp));
        }
    }

    internal static class AssemblyExtentions
    {
        public static bool DependsOn(this Assembly assembly, Assembly otherAssembly)
        {
            var referencedAssemblies = assembly.GetReferencedAssemblies().Select(a => a.FullName);
            return referencedAssemblies.Contains(otherAssembly.GetName().FullName);
        }
    }
}
