﻿using System;
using System.IO;
using System.Linq;
using Exchange.OpenExchangeClient;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.OpenExchangeClient
{
    [TestClass]
    public class CurrencyResponseParserTest
    {
        private const string sampleCurrenciesResponse = "currencies.json";

        [TestMethod]
        [DeploymentItem(@"OpenExchangeClient/currencies.json")]
        public void Parse()
        {
            Assert.IsTrue(File.Exists(sampleCurrenciesResponse), "File 'currencies.json' not found");
            var responseText = File.ReadAllText(sampleCurrenciesResponse);
            var log = new Mock<ILog>(MockBehavior.Strict);
            var currenies = new CurrencyResponseParser(log.Object).Parse(responseText);
            Assert.AreEqual(166, currenies.Length);

            var usd = currenies.First(c => c.Code == "USD");
            Assert.AreEqual("United States Dollar", usd.Name);

            var eur = currenies.First(c => c.Code == "EUR");
            Assert.AreEqual("Euro", eur.Name);

            var rub = currenies.First(c => c.Code == "RUB");
            Assert.AreEqual("Russian Ruble", rub.Name);
        }

        [TestMethod]
        public void ParseError()
        {
            var log = new Mock<ILog>();

            var currencies = new CurrencyResponseParser(log.Object).Parse("some bullshit");
            Assert.IsNotNull(currencies);
            Assert.AreEqual(0, currencies.Count());
            
            log.Verify(m => m.Error("Can't parse currency response", It.IsAny<Exception>()));
            log.VerifyAll();
        }
    }
}
