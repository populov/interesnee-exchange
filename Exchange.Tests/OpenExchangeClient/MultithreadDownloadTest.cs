﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using Exchange.OpenExchangeClient;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.OpenExchangeClient
{
    [TestClass]
    public class MultithreadDownloadTest
    {
        private static readonly DateTime yesterday = DateTime.Today.AddDays(-1);
        private static readonly DateTime today = DateTime.Today;

        [TestMethod]
        public void Download_ParallelThreads()
        {
            var config = new Mock<IRemoteRepositoryConfig>(MockBehavior.Strict);
            var openExchangeClient = MockOpenExchangeClient(MockBehavior.Strict);
            var log = new Mock<ILog>();
            var currency = TestUtils.MockCurrency("RUB", 1);

            config.Setup(m => m.TimeoutMilliseconds).Returns(3000);
            var dates = Dates.Range(today.AddDays(-4), today);
            // There will be 5 "download" threads with delays 200, 400, 600, 800, 1000 milliseconds
            // They must execute in parallel and finish in ~1 second (we'll give them 1.3 seconds).
            openExchangeClient.Setup(m => m.GetRates(currency, It.IsAny<DateTime>()))
                .Returns<ICurrency,DateTime>((curr, day) =>
                {
                    var daysBack = today.Subtract(day).Days;
                    Thread.Sleep(200 * (1+daysBack));
                    return new [] {new RateRemote("USD", curr.code, day, 30+daysBack)};
                });

            var downloader = new MultithreadRatesDownloader(openExchangeClient.Object, config.Object, log.Object);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = downloader.Download(currency, dates);
            stopwatch.Stop();

            Assert.AreEqual(5, result.Length);
            Assert.IsTrue(result.HasRate("USD", "RUB", today, 30));
            Assert.IsTrue(result.HasRate("USD", "RUB", today.AddDays(-1), 31));
            Assert.IsTrue(result.HasRate("USD", "RUB", today.AddDays(-2), 32));
            Assert.IsTrue(result.HasRate("USD", "RUB", today.AddDays(-3), 33));
            Assert.IsTrue(result.HasRate("USD", "RUB", today.AddDays(-4), 34));

            log.Verify(m => m.Info("Downloading USD-based rates " + Dates.ToString(dates)));
            log.VerifyAll();

            Assert.IsTrue(stopwatch.ElapsedMilliseconds > 1000 && stopwatch.ElapsedMilliseconds < 1300, "Expected run time 1-1.3 second, actual: " + stopwatch.ElapsedMilliseconds + "ms");
        }

        [TestMethod]
        public void Download_SumultaneousClients_GetSameData()
        {
            // If one thread requests exchange rates, expected by another thread,
            // no new download thread created, both threads will get same data.
            var config = new Mock<IRemoteRepositoryConfig>(MockBehavior.Strict);
            var openExchangeClient = MockOpenExchangeClient(MockBehavior.Strict);
            var currency = TestUtils.MockCurrency("RUB", 1);
            var log = new Mock<ILog>();

            config.Setup(m => m.TimeoutMilliseconds).Returns(3000);
            var resultRate = new RateRemote("USD", currency.code, today, 30);
            openExchangeClient.Setup(m => m.GetRates(currency, today))
                .Returns(() => { Thread.Sleep(1000); return new[] {resultRate}; });

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            RateRemote[] result1 = null;
            var thread1 = new Thread(() => result1 = ClientSimulator(1050, openExchangeClient.Object, config.Object, log.Object, currency));
            thread1.Start();

            Thread.Sleep(500);
            RateRemote[] result2 = null;
            var thread2 = new Thread(() => result2 = ClientSimulator(550, openExchangeClient.Object, config.Object, log.Object, currency));
            thread2.Start();

            thread1.Join(2000);
            thread2.Join(2000);
            stopwatch.Stop();

            Assert.IsNotNull(result1);
            Assert.IsNotNull(result2);
            Assert.AreEqual(currency.code, result1[0].Dest);
            Assert.AreEqual(result1.Length, result2.Length);
            for (int i = 0; i < result1.Length; i++)
                Assert.AreSame(result1[i], result2[i]);

            openExchangeClient.Verify(m => m.GetRates(currency, today), Times.Once());
            Assert.IsTrue(stopwatch.ElapsedMilliseconds > 1000 && stopwatch.ElapsedMilliseconds < 1300, "Expected run time 1-1.3 second, actual: " + stopwatch.ElapsedMilliseconds + "ms");
        }

        [TestMethod]
        public void Download_ExceptionsInParallelThreads_ProcessedSafely()
        {
            var config = new Mock<IRemoteRepositoryConfig>();
            var openExchangeClient = MockOpenExchangeClient(MockBehavior.Strict);
            var currency = TestUtils.MockCurrency("RUB", 1);
            var log = new Mock<ILog>();

            config.Setup(m => m.TimeoutMilliseconds).Returns(3000);
            openExchangeClient.Setup(m => m.GetRates(currency, today))
                .Returns<ICurrency, DateTime>((curr,day) => { Thread.Sleep(500); return new[] { new RateRemote("USD", curr.code, day, 30) }; });

            openExchangeClient.Setup(m => m.GetRates(currency, yesterday))
                .Returns(() => { Thread.Sleep(300); throw new InvalidOperationException("Something gone wrong"); });

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = new MultithreadRatesDownloader(openExchangeClient.Object, config.Object, log.Object).Download(currency, new[] {yesterday, today});
            stopwatch.Stop();
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Length);
            Assert.AreEqual(today, result[0].Day);

            log.Verify(m => m.Info("Downloading USD-based rates " + Dates.ToString(new[] { yesterday, today })));
            log.Verify(m => m.Error(string.Format("Error downloading rates USD per {0:d}", yesterday), It.IsAny<InvalidOperationException>()));
            log.VerifyAll();
            openExchangeClient.VerifyAll();
            Assert.IsTrue(stopwatch.ElapsedMilliseconds > 500 && stopwatch.ElapsedMilliseconds < 800, "Expected run time 500-800 milliseconds, actual: " + stopwatch.ElapsedMilliseconds + "ms");
        }

        [TestMethod]
        public void Download_Timeouts_TotalExecutionTimeLimited()
        {
            var config = new Mock<IRemoteRepositoryConfig>(MockBehavior.Strict);
            var openExchangeClient = MockOpenExchangeClient(MockBehavior.Strict);
            var currency = TestUtils.MockCurrency("RUB", 1);
            var log = new Mock<ILog>();

            config.Setup(m => m.TimeoutMilliseconds).Returns(1000);
            openExchangeClient.Setup(m => m.GetRates(currency, today))
                .Returns<ICurrency,DateTime>((curr, day) => { Thread.Sleep(500); return new[] { new RateRemote("USD", curr.code, day, 30) }; });

            openExchangeClient.Setup(m => m.GetRates(currency, yesterday))
                .Returns<ICurrency,DateTime>((curr, day) => { Thread.Sleep(3000); return new[] { new RateRemote("USD", curr.code, day, 31) }; });

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = new MultithreadRatesDownloader(openExchangeClient.Object, config.Object, log.Object).Download(currency, new[] { yesterday, today });
            stopwatch.Stop();
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Length);
            Assert.AreEqual(today, result[0].Day);

            log.Verify(m => m.Info("Downloading USD-based rates " + Dates.ToString(new[] { yesterday, today })));
            log.Verify(m => m.Warn(string.Format("Downloading rates USD per {0:d} timed out, skipping", yesterday)));
            log.VerifyAll();
            openExchangeClient.VerifyAll();
            Assert.IsTrue(stopwatch.ElapsedMilliseconds > 1000 && stopwatch.ElapsedMilliseconds < 1300, "Expected run time 1-1.3 seconds, actual: " + stopwatch.ElapsedMilliseconds + "ms");
        }


        private static RateRemote[] ClientSimulator(int expectedTimeToWait, IOpenExchangeClient openExchangeClient, IRemoteRepositoryConfig config, ILog log, ICurrency currency)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var result = new MultithreadRatesDownloader(openExchangeClient, config, log).Download(currency, new[] {today});
            stopwatch.Stop();
            const int acceptableDelay = 200;
            if (stopwatch.ElapsedMilliseconds <= expectedTimeToWait + acceptableDelay &&
                stopwatch.ElapsedMilliseconds >= expectedTimeToWait - acceptableDelay)
                return result;
            var message = string.Format("Expected time to wait = {0}, actual = {1}", expectedTimeToWait, stopwatch.ElapsedMilliseconds);
            Console.WriteLine(message);
            throw new TimeoutException(message);
        }

        private Mock<IOpenExchangeClient> MockOpenExchangeClient(MockBehavior behavior)
        {
            var openExchangeClient = new Mock<IOpenExchangeClient>(behavior);
            openExchangeClient.Setup(m => m.GetDownloadKey(It.IsAny<ICurrency>(), It.IsAny<DateTime>()))
                .Returns<ICurrency, DateTime>((curr, day) => new DownloadKey(day, "USD"));
            return openExchangeClient;
        }
}

    static class RatesExtentions
    {
        public static bool HasRate(this IEnumerable<RateRemote> rates, string from, string to, DateTime date, int val)
        {
            return rates.Any(r => r.Src == from && r.Dest == to && r.Day == date && Math.Abs(r.Val - val) < 0.0000001);
        }
    }
}
