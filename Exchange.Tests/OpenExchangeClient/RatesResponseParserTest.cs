﻿using System;
using System.IO;
using System.Linq;
using Exchange.OpenExchangeClient;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.OpenExchangeClient
{
    [TestClass]
    public class RatesResponseParserTest
    {
        private const string sampleRatesResponse = "latest.json";

/*
        [ClassInitialize]
        public static void CopySampleFile(TestContext context)
        {
            const string sampleResponse = @"..\..\..\Exchange.Tests\OpenExchangeClient\latest.json";
            Assert.IsTrue(File.Exists(sampleResponse));
            File.Copy(sampleResponse, Path.Combine(Environment.CurrentDirectory, sampleRatesResponse));
        }
*/

        [TestMethod]
        [DeploymentItem(@"OpenExchangeClient/latest.json")]
        public void ParseResopnse()
        {
            Assert.IsTrue(File.Exists(sampleRatesResponse), "File 'latest.json' not found");
            var responseText = File.ReadAllText(sampleRatesResponse);

            var log = new Mock<ILog>(MockBehavior.Strict);

            var rates = new RatesResponseParser(log.Object).Parse(responseText, DateTime.Today);
            Assert.IsNotNull(rates);
            Assert.AreEqual(166, rates.Count());

            // USD -> EUR
            var rate1 = rates.First(r => r.Dest == "EUR");
            Assert.AreEqual("USD", rate1.Src);
            Assert.AreEqual("EUR", rate1.Dest);
            Assert.AreEqual(DateTime.Today, rate1.Day);
            Assert.AreEqual(0.731128, rate1.Val, 0.000002);

            // USD -> RUB
            var rate2 = rates.First(r => r.Dest == "RUB");
            Assert.AreEqual("USD", rate2.Src);
            Assert.AreEqual("RUB", rate2.Dest);
            Assert.AreEqual(DateTime.Today, rate2.Day);
            Assert.AreEqual(34.52712, rate2.Val, 0.000002);

            // USD -> USD
            var rate3 = rates.First(r => r.Dest == "USD");
            Assert.AreEqual("USD", rate3.Src);
            Assert.AreEqual("USD", rate3.Dest);
            Assert.AreEqual(DateTime.Today, rate3.Day);
            Assert.AreEqual(1, rate3.Val);
        }

        [TestMethod]
        public void ParseError()
        {
            var log = new Mock<ILog>();

            var rates = new RatesResponseParser(log.Object).Parse("some bullshit", DateTime.Today);
            Assert.IsNotNull(rates);
            Assert.AreEqual(0, rates.Count());

            log.Verify(m => m.Error("Can't parse rates response", It.IsAny<Exception>()));
        }
    }
}
