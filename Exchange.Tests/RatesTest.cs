﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exchange.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Exchange.Tests
{
    [TestClass]
    public class RatesTest
    {
        private static readonly DateTime beforeyesterday = DateTime.Today.AddDays(-2);
        private static readonly DateTime yesterday = DateTime.Today.AddDays(-1);
        private static readonly DateTime today = DateTime.Today;

        [TestMethod]
        public void GetRatesHistory_Merge_Priority()
        {
            var pendingSave = new[] { TestUtils.MockRate(1, 2, today, 20) };
            var stored = new[] { TestUtils.MockRate(1, 2, today, 15), TestUtils.MockRate(1, 2, yesterday, 10) };

            var result = Rates.Merge(pendingSave, stored).ToArray();

            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(20, GetRate(today, result).val);
            Assert.AreEqual(10, GetRate(yesterday, result).val);
        }

        [TestMethod]
        public void GetArraingedArray_ZeroMissingAndSort()
        {
            var currencyRepo = TestUtils.MockCurrencyRepository("RUB", "EUR").Object;
            var rub = currencyRepo.GetCurrency("RUB");
            var eur = currencyRepo.GetCurrency("EUR");
            var rates = new[]
            {
                TestUtils.MockRate(rub.curID, eur.curID, today, 40),
                TestUtils.MockRate(rub.curID, eur.curID, yesterday, 41)
            };

            var result = Rates.GetArraingedArray(rates, rub, eur, beforeyesterday, today);
            
            Assert.AreEqual(3, result.Length);
            Assert.AreEqual(0, result[0]);  // Before Yesterday
            Assert.AreEqual(41, result[1]); // Yesterday
            Assert.AreEqual(40, result[2]); // Today
        }

        [TestMethod]
        public void GetArraingedArray_SortOnly()
        {
            var currencyRepo = TestUtils.MockCurrencyRepository("RUB", "EUR").Object;
            var rub = currencyRepo.GetCurrency("RUB");
            var eur = currencyRepo.GetCurrency("EUR");
            var rates = new[]
            {
                TestUtils.MockRate(rub.curID, eur.curID, today, 40),
                TestUtils.MockRate(rub.curID, eur.curID, beforeyesterday, 42),
                TestUtils.MockRate(rub.curID, eur.curID, yesterday, 41)
            };

            var result = Rates.GetArraingedArray(rates, rub, eur, beforeyesterday, today);

            Assert.AreEqual(3, result.Length);
            Assert.AreEqual(42, result[0]);  // Before Yesterday
            Assert.AreEqual(41, result[1]); // Yesterday
            Assert.AreEqual(40, result[2]); // Today
        }

        private IRate GetRate(DateTime day, IEnumerable<IRate> rates)
        {
            return rates.First(r => r.day == day);
        }
    }
}
