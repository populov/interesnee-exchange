﻿using Exchange.Model;
using Exchange.Model.Syncronization;
using Exchange.Model.OpenExchangeClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.Syncronization
{
    [TestClass]
    public class CurrencyUpdaterTest
    {
        [TestMethod]
        public void Sync()
        {
            var repository = new Mock<ICurrencyRepositoryWritable>();
            var client = new Mock<IOpenExchangeClient>(MockBehavior.Strict);

            client.Setup(m => m.GetCurrencies())
                .Returns(new[] {
                    new CurrencyRemote("EUR", "Euro"),
                    new CurrencyRemote("RUB", "Ruble")});

            var updater = new CurrencyUpdater(repository.Object, client.Object);
            updater.Sync();

            repository.Verify(m => m.InsertOrUpdate("EUR", "Euro"));
            repository.Verify(m => m.InsertOrUpdate("RUB", "Ruble"));
        }
    }
}
