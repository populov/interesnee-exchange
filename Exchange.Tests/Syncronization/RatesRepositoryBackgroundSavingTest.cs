﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exchange.Model;
using Exchange.Model.Syncronization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.Syncronization
{
    [TestClass]
    public class RatesRepositoryBackgroundSavingTest
    {
        private static readonly DateTime yesterday = DateTime.Today.AddDays(-1);
        private static readonly DateTime today = DateTime.Today;

        [TestMethod]
        public void GetRatesHistory_Merge_PendingAndStored()
        {
            var ratesSavingQueue = new Mock<IRatesSavingQueue>();
            var ratesRepository = new Mock<IRatesRepository>(MockBehavior.Strict);
            var currencies = TestUtils.MockCurrencyRepository("EUR", "USD").Object;
            var from = currencies.GetCurrency("EUR");
            var to = currencies.GetCurrency("USD");

            ratesSavingQueue.Setup(m => m.GetFromQueue(from.code, to.code, today)).Returns(1.51f);
            ratesRepository.Setup(m => m.GetRatesHistory(from, to, yesterday, today))
                .Returns(new[] {TestUtils.MockRate(from.curID, to.curID, yesterday, 1.5f)});

            var repo = new RatesRepositoryBackgroundSaving(ratesRepository.Object, ratesSavingQueue.Object);
            var result = repo.GetRatesHistory(currencies.GetCurrency("EUR"), currencies.GetCurrency("USD"), yesterday, today).ToArray();

            Assert.AreEqual(2, result.Length);
            Assert.AreEqual(1.5f, GetRate(yesterday, result).val);
            Assert.AreEqual(1.51f, GetRate(today, result).val);

            ratesSavingQueue.VerifyAll();
        }

        private IRate GetRate(DateTime day, IEnumerable<IRate> rates)
        {
            return rates.First(r => r.day == day);
        }
    }
}
