﻿using System;
using System.Linq;
using Exchange.Model;
using Exchange.Model.Syncronization;
using Exchange.Model.OpenExchangeClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.Syncronization
{
    [TestClass]
    public class RatesRepositoryCompositeTest
    {
        private readonly DateTime today = DateTime.Today;
        private readonly DateTime yesterday = DateTime.Today.AddDays(-1);

        [TestMethod]
        public void GetRatesHistory_MergeAndSave()
        {
            var eur = TestUtils.MockCurrency("EUR", 1);
            var rub = TestUtils.MockCurrency("RUB", 2);
            var ratesRepository = new Mock<IRatesRepository>(MockBehavior.Strict);
            var downloader = new Mock<IBatchRatesDownloader>(MockBehavior.Strict);
            var ratesSaver = new Mock<IRatesSaver>();

            ratesRepository.Setup(m => m.GetRatesHistory(eur, rub, yesterday, today))
                .Returns(new[] {TestUtils.MockRate(eur.curID, rub.curID, yesterday, 30)});

            var ratesRemote = new[] {new RateRemote("EUR", "RUB", today, 31)};
            downloader.Setup(m => m.Download(eur, new[] {today})).Returns(ratesRemote);

            var compositeRepo = new RatesRepositoryComposite(ratesRepository.Object, downloader.Object, ratesSaver.Object);
            var rates = compositeRepo.GetRatesHistory(eur, rub, yesterday, today).ToArray();

            Assert.AreEqual(2, rates.Length);
            Assert.AreEqual(30, rates.Single(r => r.day == yesterday).val);
            Assert.AreEqual(31, rates.Single(r => r.day == today).val);

            ratesSaver.Verify(m => m.SaveRatesInBackground(It.Is<RateRemote[]>(saveResult =>
                saveResult.Count() == 1
                && saveResult[0].Src == eur.code
                && saveResult[0].Dest == rub.code
                && saveResult[0].Day.Equals(today)
                && Math.Abs(saveResult[0].Val - 31) < 0.00000001
            )));
        }
    }
}
