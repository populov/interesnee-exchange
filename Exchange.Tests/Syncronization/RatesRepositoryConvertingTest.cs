﻿using System;
using System.Collections.Generic;
using System.Linq;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using Exchange.Model.Syncronization;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.Syncronization
{
    [TestClass]
    public class RatesRepositoryConvertingTest
    {
        private readonly DateTime today = DateTime.Today;
        private readonly DateTime yesterday = DateTime.Today.AddDays(-1);
        private readonly DateTime beforeYesterday = DateTime.Today.AddDays(-2);

        [TestMethod]
        public void GetRatesHistory_MergeAndSave()
        {
            var eur = TestUtils.MockCurrency("EUR", 1);
            var rub = TestUtils.MockCurrency("RUB", 2);
            var usd = TestUtils.MockCurrency("USD", 3);
            var ratesRepository = new Mock<IRatesRepository>(MockBehavior.Strict);
            var currencyRepository = new Mock<ICurrencyRepository>(MockBehavior.Strict);
            var ratesSaver = new Mock<IRatesSaver>();
            var log = new Mock<ILog>();

            currencyRepository.Setup(m => m.GetCurrency("USD")).Returns(usd);

            ratesRepository.Setup(m => m.GetRatesHistory(eur, rub, beforeYesterday, today))
                .Returns(new[] { TestUtils.MockRate(eur.curID, rub.curID, beforeYesterday, 40) });

            ratesRepository.Setup(m => m.GetRatesHistory(usd, eur, beforeYesterday, today ))
                .Returns(new[] {
                    TestUtils.MockRate(usd.curID, eur.curID, yesterday, 2f/3),
                    TestUtils.MockRate(usd.curID, eur.curID, today, 0.8f)
                });

            ratesRepository.Setup(m => m.GetRatesHistory(usd, rub, beforeYesterday, today))
                .Returns(new[] {
                    TestUtils.MockRate(usd.curID, rub.curID, yesterday, 30),
                    TestUtils.MockRate(usd.curID, rub.curID, today, 40)
                });

            var convertingRepo = new RatesRepositoryConverting(ratesRepository.Object, ratesSaver.Object, currencyRepository.Object, log.Object);
            var rates = convertingRepo.GetRatesHistory(eur, rub, beforeYesterday, today).ToArray();

            Assert.AreEqual(3, rates.Length);
            Assert.AreEqual(40, rates.Single(r => r.day == beforeYesterday).val);
            Assert.AreEqual(45, rates.Single(r => r.day == yesterday).val);
            Assert.AreEqual(50, rates.Single(r => r.day == today).val, 0.000005);

            Func<IEnumerable<RateRemote>, bool> verifySave = items =>
            {
                var itemsToSave = items.ToArray();
                var result = itemsToSave.Length == 2;
                var rate1 = itemsToSave.Single(r => r.Day == yesterday);
                result &= rate1.Src == "EUR"
                          && rate1.Dest == "RUB"
                          && Math.Abs(rate1.Val - 45) < 0.000005;
                var rate2 = itemsToSave.Single(r => r.Day == today);
                result &= rate2.Src == "EUR"
                          && rate2.Dest == "RUB"
                          && Math.Abs(rate2.Val - 50) < 0.000005;
                return result;
            };
            ratesSaver.Verify(m => m.SaveRatesInBackground(It.Is<IEnumerable<RateRemote>>(items => verifySave(items))), "Save items wasn't called properly");
            ratesSaver.VerifyAll();
            log.Verify(m => m.Info("Converted EUR->RUB: "+ Dates.ToString(new[] {yesterday, today})));
            log.VerifyAll();
        }
    }
}
