﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using Exchange.Model.Syncronization;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Exchange.Tests.Syncronization
{
    [TestClass]
    public class RatesSaverSingleThreadTest
    {
        static readonly Mock<ICurrencyRepository> currencyRepository = TestUtils.MockCurrencyRepository("USD", "RUB", "EUR");
        readonly ICurrency usd = currencyRepository.Object.GetCurrency("USD");
        readonly ICurrency rub = currencyRepository.Object.GetCurrency("RUB");
        readonly ICurrency eur = currencyRepository.Object.GetCurrency("EUR");

        [TestMethod]
        public void SaveInBackground_SumultaneousAdd_SingleSave()
        {
            // When added element is already in queue, it's ignored
            var ratesRepository = new Mock<IRatesRepositoryWritable>(MockBehavior.Strict);
            var currencyRepositoryW = new Mock<ICurrencyRepositoryWritable>(MockBehavior.Strict);
            var openExchangeClient = new Mock<IOpenExchangeClient>(MockBehavior.Strict);
            var currencyUpdater = new CurrencyUpdater(currencyRepositoryW.Object, openExchangeClient.Object);
            var log = new Mock<ILog>();

            ratesRepository.Setup(m => m.InsertOrUpdate(It.IsAny<ICurrency>(), It.IsAny<ICurrency>(), It.IsAny<DateTime>(), It.IsAny<double>()))
                .Callback(() => Thread.Sleep(100));

            var saver = new RatesSaverSingleThread(ratesRepository.Object, currencyRepository.Object, currencyUpdater, log.Object);

            ThreadStart addData = () =>
            {
                var ratesToSave = new[] { new RateRemote("USD", "RUB", DateTime.Today, 30), new RateRemote("EUR", "RUB", DateTime.Today, 45) };
                saver.SaveRatesInBackground(ratesToSave);
            };

            for(int i = 0; i < 5; i++)
                new Thread(addData).Start();
            Thread.Sleep(500);

            log.Verify(m => m.Info("Saving 2 new rates"), Times.Exactly(5));
            log.VerifyAll();
            ratesRepository.Verify(m => m.InsertOrUpdate(usd, rub, DateTime.Today, 30), Times.Once);
            ratesRepository.Verify(m => m.InsertOrUpdate(eur, rub, DateTime.Today, 45), Times.Once);
        }

        [TestMethod]
        public void SaveInBackground_SequentialAdd_MultipleSave()
        {
            // Adding same element after previous was processed saves element again
            var ratesRepository = new Mock<IRatesRepositoryWritable>(MockBehavior.Strict);
            var openExchangeClient = new Mock<IOpenExchangeClient>(MockBehavior.Strict);
            var currencyRepositoryW = new Mock<ICurrencyRepositoryWritable>(MockBehavior.Strict);
            var currencyUpdater = new CurrencyUpdater(currencyRepositoryW.Object, openExchangeClient.Object);
            var log = new Mock<ILog>();
            var saveThreads = new List<int>();

            ratesRepository.Setup(m => m.InsertOrUpdate(It.IsAny<ICurrency>(), It.IsAny<ICurrency>(), It.IsAny<DateTime>(), It.IsAny<double>()))
                .Callback(() => saveThreads.Add(Thread.CurrentThread.ManagedThreadId));

            var saver = new RatesSaverSingleThread(ratesRepository.Object, currencyRepository.Object, currencyUpdater, log.Object);

            ThreadStart addData = () =>
            {
                var ratesToSave = new[] { new RateRemote("USD", "RUB", DateTime.Today, 30), new RateRemote("EUR", "RUB", DateTime.Today, 45) };
                saver.SaveRatesInBackground(ratesToSave);
            };

            for (int i = 0; i < 3; i++)
            {
                new Thread(addData).Start();
                Thread.Sleep(100);
            }

            Assert.IsTrue(1 < saveThreads.Distinct().Count());
            log.Verify(m => m.Info("Saving 2 new rates"), Times.Exactly(3));
            log.VerifyAll();
            ratesRepository.Verify(m => m.InsertOrUpdate(usd, rub, DateTime.Today, 30), Times.Exactly(3));
            ratesRepository.Verify(m => m.InsertOrUpdate(eur, rub, DateTime.Today, 45), Times.Exactly(3));
        }


    }
}
