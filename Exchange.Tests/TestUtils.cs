﻿using System;
using Exchange.Model;
using Moq;

namespace Exchange.Tests
{
    public static class TestUtils
    {
        public static ICurrency MockCurrency(string code, int currencyId)
        {
            var currency = new Mock<ICurrency>();
            currency.Setup(m => m.code).Returns(code);
            currency.Setup(m => m.curID).Returns(currencyId);
            return currency.Object;
        }

        public static IRate MockRate(int src, int dest, DateTime day, double val)
        {
            var rate = new Mock<IRate>();
            rate.Setup(m => m.src).Returns(src);
            rate.Setup(m => m.dest).Returns(dest);
            rate.Setup(m => m.day).Returns(day);
            rate.Setup(m => m.val).Returns(val);
            return rate.Object;
        }

        public static Mock<ICurrencyRepository> MockCurrencyRepository(params string[] currencyCode)
        {
            var result = new Mock<ICurrencyRepository>();
            for (int i = 0; i < currencyCode.Length; i++)
            {
                var code = currencyCode[i];
                var currency = MockCurrency(code, i + 1);
                result.Setup(m => m.GetCurrency(code)).Returns(currency);
            }
            return result;
        }
    }
}
