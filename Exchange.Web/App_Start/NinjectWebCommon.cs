using System;
using System.Web;
using System.Web.Http;
using Exchange;
using Exchange.Model;
using Exchange.Model.OpenExchangeClient;
using Exchange.Model.Syncronization;
using Exchange.Models;
using Exchange.OpenExchangeClient;
using Exchange.Storage;
using log4net;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;

[assembly: WebActivator.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace Exchange
{
    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            GlobalConfiguration.Configuration.DependencyResolver = new Bugfixes.NinjectDependencyResolver(kernel);

            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IExchangeUIConfig>().To<ExchangeUIConfiguration>().InSingletonScope();
            kernel.Bind<IRemoteRepositoryConfig>().To<RemoteRepositoryConfig>().InSingletonScope();
            kernel.Bind<ICurrencyRepository>().To<CachedCurrencyRepository>().InSingletonScope();
            kernel.Bind<ICurrencyRepositoryWritable>().To<CachedCurrencyRepository>().InSingletonScope();
            kernel.Bind<ICurrencyRepositoryWritable>().To<DbRepository>().WhenInjectedInto<CachedCurrencyRepository>();
            kernel.Bind<IRatesRepository>().To<RatesRepositoryComposite>();
            kernel.Bind<IRatesRepository>().To<RatesRepositoryConverting>().WhenInjectedInto<RatesRepositoryComposite>();
            kernel.Bind<IRatesRepository>().To<RatesRepositoryBackgroundSaving>().WhenInjectedInto<RatesRepositoryConverting>();
            kernel.Bind<IRatesRepository>().To<DbRepository>().WhenInjectedInto<RatesRepositoryBackgroundSaving>();
            kernel.Bind<IRatesRepositoryWritable>().To<DbRepository>().WhenInjectedInto<RatesSaverSingleThread>();
            kernel.Bind<IRatesSaver>().To<RatesRepositoryBackgroundSaving>();
            kernel.Bind<IRatesSavingQueue>().To<RatesSaverSingleThread>();
            kernel.Bind<IOpenExchangeClient>().To<OpenExchangeClientFree>().InRequestScope();
            kernel.Bind<IOpenExchangeClient>()
                .To<BreakLoopClient>()
                .WhenInjectedInto<CurrencyUpdater>()
                .InSingletonScope();
            kernel.Bind<IBatchRatesDownloader>().To<MultithreadRatesDownloader>();

            log4net.Config.XmlConfigurator.Configure();
            kernel.Bind<ILog>().ToMethod(context => LogManager.GetLogger(context.Request.Target.Member.ReflectedType)).InRequestScope();
        }        
    }
}
