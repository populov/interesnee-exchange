﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web.Http.Dependencies;
using Ninject;
using Ninject.Activation;
using Ninject.Parameters;
using Ninject.Syntax;

namespace Exchange.Bugfixes
{
    public class NinjectDependencyScope : IDependencyScope
    {
        private IResolutionRoot resolver;

        internal NinjectDependencyScope(IResolutionRoot resolver)
        {
            Contract.Assert(resolver != null);
            this.resolver = resolver;
        }

        public void Dispose()
        {
            var disposable = resolver as IDisposable;
            if (disposable != null)
                disposable.Dispose();

            resolver = null;
        }

        public object GetService(Type serviceType)
        {
            if (resolver == null)
                throw new ObjectDisposedException("this", "This scope has already been disposed");

            IRequest request = resolver.CreateRequest(serviceType, null, new Parameter[0], true, true);
            try
            {
                return resolver.Resolve(request).SingleOrDefault();
            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Fatal("Can't resolsve dependency", e);
                throw;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (resolver == null)
                throw new ObjectDisposedException("this", "This scope has already been disposed");

            IRequest request = resolver.CreateRequest(serviceType, null, new Parameter[0], true, true);
            try
            {
                return resolver.Resolve(request).ToList();
            }
            catch (Exception e)
            {
                log4net.LogManager.GetLogger(GetType()).Fatal("Can't resolve dependency", e);
                throw;
            }
        }
    }

    public class NinjectDependencyResolver : NinjectDependencyScope, IDependencyResolver
    {
        private readonly IKernel kernel;

        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            this.kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(kernel.BeginBlock());
        }
    }
}