﻿using System.Configuration;
using Exchange.Config;

namespace Exchange.Models
{
    public class ExchangeUIConfiguration : IExchangeUIConfig
    {
        public ExchangeUIConfiguration()
        {
            var section = (ExchangeUIConfigurationSection) ConfigurationManager.GetSection("exchange.web.ui");
            DateFormat = section.DateFormat;
            MaxRangeInDays = section.MaxRangeDays;
        }

        public string DateFormat { get; private set; }
        public int MaxRangeInDays { get; private set; }
    }
}