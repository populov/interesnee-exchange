﻿using System.Configuration;

namespace Exchange.Config
{
    public class ExchangeUIConfigurationSection : ConfigurationSection
    {
        private const string maxRangeDays = "maxRangeDays";
        private const string dateFormat = "dateFormat";

        [ConfigurationProperty(dateFormat, IsRequired = false, DefaultValue = "dd-mm-yyyy")]
        public string DateFormat
        {
            get { return (string) this[dateFormat]; }
        }

        [ConfigurationProperty(maxRangeDays, IsRequired = false, DefaultValue = 61)]
        public int MaxRangeDays 
        {
            get { return (int)this[maxRangeDays]; }
        }
    }
}