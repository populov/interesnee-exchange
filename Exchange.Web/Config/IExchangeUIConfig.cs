﻿namespace Exchange.Models
{
    public interface IExchangeUIConfig
    {
        string DateFormat { get; }
        int MaxRangeInDays { get; }
    }
}
