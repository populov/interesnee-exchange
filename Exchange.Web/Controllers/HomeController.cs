﻿using System.Diagnostics.Contracts;
using System.Web.Mvc;
using Exchange.Model;
using Exchange.Models;

namespace Exchange.Controllers
{
    public class HomeController : Controller
    {
        private readonly IExchangeUIConfig config;
        private readonly ICurrencyRepository currencyRepository;

        public HomeController(IExchangeUIConfig config, ICurrencyRepository currencyRepository)
        {
            Contract.Requires(config != null);
            Contract.Requires(currencyRepository != null);

            this.config = config;
            this.currencyRepository = currencyRepository;
        }

        public ActionResult Index()
        {
            return View(new HomeViewModel(config, currencyRepository));
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}