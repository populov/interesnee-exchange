﻿using System;
using System.Data;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.IO;
using System.Web.Http;
using Exchange.Model;
using Exchange.Models;

namespace Exchange.Controllers
{
    public class RatesController : ApiController
    {
        private readonly IRatesRepository ratesRepository;
        private readonly ICurrencyRepository currencyRepository;
        private readonly int maxRangeInDays;

        public RatesController(IRatesRepository ratesRepository, ICurrencyRepository currencyRepository, IExchangeUIConfig config)
        {
            Contract.Requires<ArgumentNullException>(ratesRepository != null);
            Contract.Requires<ArgumentNullException>(currencyRepository != null);
            Contract.Requires<ArgumentNullException>(config != null);

            this.ratesRepository = ratesRepository;
            this.currencyRepository = currencyRepository;
            maxRangeInDays = config.MaxRangeInDays;
        }

        [Route(@"api/rates/{fromCurrencyCode}-{toCurrencyCode}/{sinceStr:regex(^\d{4}-\d\d-\d\d$)}/{upToStr:regex(^\d{4}-\d\d-\d\d$)}")]
        public IHttpActionResult GetAllRates(string fromCurrencyCode, string toCurrencyCode, string sinceStr, string upToStr)
        {
            var since = ParseDate(sinceStr);
            var upTo = ParseDate(upToStr);
            if (upTo.Subtract(since).TotalDays > maxRangeInDays)
                throw new InvalidDataException("Too long period selected");

            var fromCurrency = currencyRepository.GetCurrency(fromCurrencyCode);
            if (fromCurrency == null)
                throw new NullReferenceException("Unknown currency: " + fromCurrencyCode);

            var toCurrency = currencyRepository.GetCurrency(toCurrencyCode);
            if (toCurrency == null)
                throw new NullReferenceException("Unknown currency: " + toCurrencyCode);

            var ratesHistory = ratesRepository.GetRatesHistory(fromCurrency, toCurrency, since, upTo);
            var data = Rates.GetArraingedArray(ratesHistory, fromCurrency, toCurrency, since, upTo);
            return Ok(data);
        }

        public static DateTime ParseDate(String dateStr)
        {
            DateTime result;
            if (!DateTime.TryParseExact(dateStr, "yyyy-MM-dd", null, DateTimeStyles.None, out result))
                throw new FormatException(dateStr + " does not match format ");
            return result;
        }
    }
}