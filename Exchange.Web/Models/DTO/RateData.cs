﻿using System;
using Exchange.Model;

namespace Exchange.Models.DTO
{
    public class RateData
    {
        public RateData(IRate rate)
        {
            date = rate.day;
            val = rate.val;
        }

        public DateTime date { get; private set; }
        public double val { get; private set; }
    }
}