﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Exchange.Model;

namespace Exchange.Models
{
    public class HomeViewModel
    {
        private readonly IExchangeUIConfig config;
        private readonly ICurrencyRepository currencyRepository;
        private readonly string innerDateFormat;

        public HomeViewModel(IExchangeUIConfig config, ICurrencyRepository currencyRepository)
        {
            Contract.Requires(config != null);
            Contract.Requires(currencyRepository != null);

            this.config = config;
            this.currencyRepository = currencyRepository;
            innerDateFormat = DateFormat.Replace("m", "M");
        }


        public string DateFormat { get { return config.DateFormat; } }

        public int MaxRangeInDays { get { return config.MaxRangeInDays; } }

        public IEnumerable<ICurrency> Currencies { get { return currencyRepository.GetAllCurrencies(); } }

        public string SinceDate { get { return DateTime.Now.Subtract(new TimeSpan(14, 0, 0, 0)).ToString(innerDateFormat); } }

        public string UpToDate { get { return DateTime.Now.ToString(innerDateFormat); } }
    }
}