﻿var pageController = (function () {
    var messageTemplate = Handlebars.compile($("#error-message").html());

    function init() {
        if (!parseUrlHash())
            chartController.updateChart();
    }

    function parseUrlHash() {
        var parts = window.location.hash.match(/^#(\w+)-(\w+)\/(.+)-(.+)$/);
        if (parts && parts.length == 5)
        {
            $("#calendarSince").datepicker("update", parts[3]);
            $("#calendarUpTo").datepicker("update", parts[4]);
            if (!currenciesController.setPair(parts[1], parts[2]))
                chartController.updateChart();
            return true;
        }
        return false;
    }

    function showMessage(message, title, type) {
        $("#messagePlaceholder").html(messageTemplate({
            type: type ? type : "warning",
            title: title ? title : "Warning:",
            message: message
        }));
    }

    function hideMessage() {
        $("#messagePlaceholder").empty();
    }

    function setProgress(show) {
        $("#btnUpdate")[0].disabled = show;
        if (show)
            $("#imgProgress").removeClass("hidden");
        else
            $("#imgProgress").addClass("hidden");
    }

    function updateAddressHash() {
        var urlHash = "#" + currenciesController.fromCurrency() + "-" + currenciesController.toCurrency()
            + "/" + datesController.sinceDate().format(datesController.dateFormat)
            + "-" + datesController.upToDate().format(datesController.dateFormat);
        window.location.href = urlHash;
    }

    return {
        init: init,
        showMessage: showMessage,
        hideMessage: hideMessage,
        setProgress: setProgress,
        updateAddressHash: updateAddressHash
    };
})();

var currenciesController = (function () {
    var itemTemplate = Handlebars.compile($("#currency-item").html());
    var searchTemplate = Handlebars.compile($("#currency-search").html());

    var currencyFrom = currencyListController(1, itemTemplate, searchTemplate, onCurrencySelected);
    var currencyTo = currencyListController(2, itemTemplate, searchTemplate, onCurrencySelected);

    $("#currencySwap").on("click", swapCurrencies);

    function onCurrencySelected(listId, selectedCurrency) {
        var otherList = 3 - listId;
        var otherCurrency = $("#currency" + otherList).text();
        if (listId == 1)
            updateCurrencies(selectedCurrency, otherCurrency);
        else
            updateCurrencies(otherCurrency, selectedCurrency);
    }

    function updateCurrencies(newFrom, newTo) {
        var oldFrom = currencyFrom.getCode();
        var oldTo = currencyTo.getCode();
        if (oldFrom == newFrom && oldTo == newTo)
            return false;
        currencyFrom.update(newFrom, newTo);
        currencyTo.update(newTo, newFrom);
        chartController.updateChart();
        return true;
    }

    function swapCurrencies() {
        updateCurrencies(currencyTo.getCode(), currencyFrom.getCode());
    }

    function getCurrencyName(code) {
        for (var i in currencies)
            if (currencies[i].code == code) {
                return currencies[i].name;
            }
        return "Unknown currency";
    }

    function currencyListController(listId, currencyItemTemplate, searchBoxTemplate, onSelect) {
        var currencyElement = $("#currency" + listId)[0];
        var thisList = $("#currencyList" + listId);
        thisList.html(searchBoxTemplate());
        thisList.append(currencyItemTemplate({ currencies: currencies }));
        var currencyItems = thisList.find(".currency-item");
        var searchBox = thisList.find(".currency-search").find("INPUT");
        currencyItems.on("click", currencyClick);
        searchBox.on({
            click: function (event) { event.stopPropagation(); },
            keyup: currencySearchTextChange
        });

        function currencyClick(event) {
            if ($(this).hasClass("disabled")) {
                event.stopPropagation();
                return;
            }
            var selectedCurrency = this.getAttribute("data-currency");
            onSelect(listId, selectedCurrency);
        }

        function update(selectedCurrency, disableCurrency) {
            $(currencyElement).text(selectedCurrency);
            currencyElement.title = getCurrencyName(selectedCurrency);
            currencyItems.removeClass("disabled");
            currencyItems.filter("[data-currency=" + disableCurrency + "]").addClass("disabled");
            clearSearchAndResetSelected();
        }

        function clearSearchAndResetSelected() {
            searchBox.val("");
            currencyItems.removeClass("hidden active");
            currencyItems.filter("[data-currency=" + getCurrencyCode() + "]").addClass("active");
        }

        function handleNavigation(event) {
            var canSelect = ".currency-item:not(.hidden):not(.disabled)";
            var selectableItems = thisList.find(canSelect);
            var activeItem = selectableItems.filter(".active");
            var newActive = [];
            switch (event.keyCode) {
                case 13: // Enter
                    activeItem.click();
                    return true;
                case 27: // Esc
                    thisList.click();
                    clearSearchAndResetSelected();
                    return true;
                case 38: // Up
                    if (activeItem.length)
                        newActive = activeItem.prevAll(canSelect).first();
                    newActive = newActive.length ? newActive : selectableItems.last();
                    break;
                case 40: // Down
                    if (activeItem.length)
                        newActive = activeItem.nextAll(canSelect).first();
                    newActive = newActive.length ? newActive : selectableItems.first();
                    break;
            }
            if (newActive.length) {
                currencyItems.removeClass("active");
                newActive.addClass("active");
                return true;
            }
            return false;
        }

        RegExp.escape = function(str) {
            if (!arguments.callee.sRE) {
                arguments.callee.sRE = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'gim');
            }
            return str.replace(arguments.callee.sRE, '\\$1');
        };

        function currencySearchTextChange(event) {
            if (handleNavigation(event))
                return;
            var searchText = this.value;
            if (!searchText) {
                currencyItems.removeClass("hidden");
                return;
            }
            var search = new RegExp(RegExp.escape(searchText), "i");
            $.each(currencyItems, function (index, currencyItem) {
                if ($(currencyItem).text().match(search))
                    $(currencyItem).removeClass("hidden");
                else
                    $(currencyItem).addClass("hidden");
            });
        }

        function getCurrencyCode() {
            return $(currencyElement).text();
        }

        return {
            getCode: getCurrencyCode,
            update: update
        };
    }

    return {
        fromCurrency: currencyFrom.getCode,
        toCurrency: currencyTo.getCode,
        setPair: updateCurrencies,
        getName: getCurrencyName
    };
})();

var datesController = (function () {
    var dayInMilliseconds = 24 * 3600 * 1000;
    var maxRangeDays = parseInt($("#maxRangeInDays").val());
    var minRangeDays = 1;
    var now = new Date();
    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0);
    var oeStartDate = new Date(1999, 0, 1); // Dates before 01.01.1999 are unavailable at OpenExchangeRates.org
    var dateFormat = $("#dateRange").attr("data-date-format");

    $('#dateRange.input-daterange').datepicker({
        todayBtn: "linked",
        todayHighlight: true,
//        startDate: oeStartDate,
//        endDate: today,
        beforeShowDay: function (date) { return date > today || date < oeStartDate ? 'disabled' : ''; },
        multidate: false,
        forceParse: false
    });
    var since = $('#calendarSince');
    since.on('changeDate', sinceDateSelected);

    var upTo = $('#calendarUpTo');
    upTo.on('changeDate', upToDateSelected);

    function sinceDateSelected(ev) {
        var sinceDate = new Date(ev.date);
        if (isDateOutOfRange(sinceDate))
            return;
        var diff = datesDiffInDays(sinceDate, getUpToDate());
        if (diff > maxRangeDays) {
            upTo.datepicker("update", new Date(Math.min(today.valueOf(), sinceDate.valueOf() + maxRangeDays * dayInMilliseconds)));
            showMaximumPeriodWarning();
        } else if (diff < minRangeDays) {
            upTo.datepicker("update", new Date(Math.min(today.valueOf(), sinceDate.valueOf() + minRangeDays * dayInMilliseconds)));
        }
        since.datepicker("hide");
        upTo[0].focus();
    }

    function upToDateSelected(ev) {
        var upToDate = new Date(ev.date);
        if (isDateOutOfRange(upToDate))
            return;
        var diff = datesDiffInDays(getSinceDate(), upToDate);
        if (diff > maxRangeDays) {
            since.datepicker("update", new Date(upToDate.valueOf() - maxRangeDays * dayInMilliseconds));
            showMaximumPeriodWarning();
            since[0].focus();
        } else if (diff < minRangeDays) {
            since.datepicker("update", new Date(upToDate.valueOf() - minRangeDays * dayInMilliseconds));
        }
        upTo.datepicker("hide");
    }

    function datesDiffInDays(earlier, later) {
        return (later.valueOf() - earlier.valueOf()) / dayInMilliseconds;
    }

    function showMaximumPeriodWarning() {
        pageController.showMessage("You cannot select period longer, than " + maxRangeDays + (maxRangeDays % 10 == 1 ? " day" : " days"));
    }

    function isDateOutOfRange(date) {
        if (date > today) {
            pageController.showMessage("Future exchange rates are unpredictable");
            return true;
        }
        if (date < oeStartDate) {
            pageController.showMessage("Rates available since " + oeStartDate.format(dateFormat));
            return true;
        }
        return false;
    }

    function verifyDates(sinceDate, upToDate) {
        if (!sinceDate || sinceDate == "Invalid Date") {
            pageController.showMessage("Enter since date");
            return false;
        }
        if (!upToDate || upToDate == "Invalid Date") {
            pageController.showMessage("Enter up to date");
            return false;
        }
        var datesDiff = datesDiffInDays(sinceDate, upToDate);
        if (datesDiff < 0) {
            pageController.showMessage("Since date must be before up to date");
            return false;
        }
        if (isDateOutOfRange(sinceDate) || isDateOutOfRange(upToDate))
            return false;
        if (datesDiff > maxRangeDays) {
            showMaximumPeriodWarning();
            return false;
        }
        return true;
    }

    function getSinceDate() {
        return since.datepicker("getDate");
    }

    function getUpToDate() {
        return upTo.datepicker("getDate");
    }

    Date.prototype.format = function (format) {
        var o = {
            "m+": this.getMonth() + 1, //month
            "d+": this.getDate(), //day
            "H+": this.getHours(), //hour
            "M+": this.getMinutes(), //minute
            "s+": this.getSeconds(), //second
            "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
            "S": this.getMilliseconds() //millisecond
        };

        if (/(y+)/.test(format))
            format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) if (new RegExp("(" + k + ")").test(format))
            format = format.replace(RegExp.$1,
              RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
        return format;
    };

    return {
        sinceDate:  getSinceDate,
        upToDate: getUpToDate,
        verifyDates: verifyDates,
        dateFormat: dateFormat
    };
})();

var chartController = (function () {
    var dayInMilliseconds = 24 * 3600 * 1000;
    $("#btnUpdate").on("click", updateChart);

    function updateChart() {
        pageController.hideMessage();
        var fromCurrency = currenciesController.fromCurrency();
        var toCurrency = currenciesController.toCurrency();
        var since = datesController.sinceDate();
        var upTo = datesController.upToDate();
        if (!datesController.verifyDates(since, upTo))
            return;

        var getRatesUrl = "/api/rates/" + fromCurrency + "-" + toCurrency + "/" + since.format("yyyy-mm-dd") + "/" + upTo.format("yyyy-mm-dd") + "?type=json";
        pageController.setProgress(true);
        $.getJSON(getRatesUrl)
            .done(function (data) {
                pageController.setProgress(false);
                pageController.updateAddressHash();
                drawChart(since, data);
                warnIfMissingDates(since, data);
            })
            .fail(function (jqxhr, textStatus, error) {
                pageController.setProgress(false);
                pageController.showMessage(error, textStatus, "danger");
            });
    }

    function drawChart(start, seriesData) {
        var chartData = {
            chart: { zoomType: 'x', spacingRight: 20 },
            title: { text: currenciesController.getName(currenciesController.fromCurrency()) + " to " + currenciesController.getName(currenciesController.toCurrency()) },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 3 * dayInMilliseconds, // 3 days
                title: { text: null }
            },
            yAxis: {
                title: { text: 'Exchange rate' }
            },
            tooltip: { shared: true },
            legend: { enabled: false },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false
                    },
                    shadow: false,
                    states: { hover: { lineWidth: 1 } },
                    threshold: null
                }
            },

            series: [{
                    type: 'area',
                    name: currenciesController.fromCurrency() + ' to ' + currenciesController.toCurrency(),
                    pointInterval: dayInMilliseconds,
                    pointStart: Date.UTC(start.getYear(), start.getMonth(), start.getDate()),
                    data: seriesData
                }]
        };
        $('#chartContainer').highcharts(chartData);
    }

    function warnIfMissingDates(since, data) {
        var start = since.valueOf();
        var missingDates = [];
        $.each(data, function (i, item) {
            if (item <= 0) {
                missingDates.push(new Date(start + i * dayInMilliseconds).format(datesController.dateFormat));
            }
        });
        if (missingDates.length > 0) {
            pageController.showMessage("Missing exchange rates for " + missingDates.join(", "));
        }
    };

    return {
        updateChart: updateChart
    };
})();

$().ready(pageController.init);