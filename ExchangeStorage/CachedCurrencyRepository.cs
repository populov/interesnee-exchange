﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Exchange.Model;

namespace Exchange.Storage
{
    public class CachedCurrencyRepository : ICurrencyRepositoryWritable
    {
        private readonly ICurrencyRepositoryWritable underlyingRepository;
        private readonly IDictionary<string, CurrencyPOCO> currencies; 

        public CachedCurrencyRepository(ICurrencyRepositoryWritable underlyingRepository)
        {
            Contract.Requires(underlyingRepository != null);
            this.underlyingRepository = underlyingRepository;

            var allCurrencies = underlyingRepository.GetAllCurrencies();
            var result = new Dictionary<string, CurrencyPOCO>();
            foreach (var c in allCurrencies)
                result[c.code] = new CurrencyPOCO(c);
            currencies = result;
        }

        public IEnumerable<ICurrency> GetAllCurrencies()
        {
            return currencies.Values;
        }

        public ICurrency GetCurrency(string code)
        {
            CurrencyPOCO result;
            if (currencies.TryGetValue(code, out result))
                return result;
            return null;
        }

        public ICurrency InsertOrUpdate(string code, string name)
        {
            var updatedCurrency = underlyingRepository.InsertOrUpdate(code, name);
            if (updatedCurrency == null)
                return null;
            var currency = new CurrencyPOCO(updatedCurrency);
            lock (currencies)
            {
                if (currencies.ContainsKey(code))
                    currencies[code] = currency;
                else
                    currencies.Add(code, currency);
            }
            return updatedCurrency;
        }

        private class CurrencyPOCO : ICurrency
        {
            public CurrencyPOCO(ICurrency source)
            {
                Contract.Requires(source != null);

                curID = source.curID;
                code = source.code;
                name = source.name;
            }

            public int curID { get; private set; }
            public string code { get; private set; }
            public string name { get; private set; }

            public override string ToString()
            {
                return string.Format("code = {1}, name = {2}, id = {0}", curID, code, name);
            }
        }
    }
}