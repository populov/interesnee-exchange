﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Diagnostics.Contracts;
using System.Linq;
using Exchange.Model;
using log4net;

namespace Exchange.Storage
{
    public class DbRepository : IRatesRepository, ICurrencyRepositoryWritable, IRatesRepositoryWritable
    {
        private readonly ILog log;
        private readonly ExchangeDBDataContext db;

        public DbRepository(ILog log)
        {
            Contract.Requires<ArgumentNullException>(log != null);
            this.log = log;

            var mainConnection = ConfigurationManager.ConnectionStrings["main"];
            var dbProviderFactory = DbProviderFactories.GetFactory(mainConnection.ProviderName);
            var dbConnection = dbProviderFactory.CreateConnection();
            if (dbConnection == null)
                throw new NullReferenceException("Can't create database connection. ProviderName='" + mainConnection.ProviderName + "'");
            dbConnection.ConnectionString = mainConnection.ConnectionString;
            db = new ExchangeDBDataContext(dbConnection);
        }

        public IEnumerable<ICurrency> GetAllCurrencies()
        {
            return db.Currencies;
        }

        public ICurrency GetCurrency(string code)
        {
            return db.Currencies.FirstOrDefault(c => c.code == code);
        }

        public ICurrency InsertOrUpdate(string code, string name)
        {
            var existing = db.Currencies.FirstOrDefault(c => c.code == code);
            if (existing == null)
            {
                log.Info(string.Format("New currency: {0} - {1}", code, name));
                db.Currencies.InsertOnSubmit(new Currency {code = code, name = name});
            }
            else if (existing.name == name)
                return existing;
            else
            {
                log.Info(string.Format("Update currency: {0} - {1}", code, name));
                existing.name = name;
            }
            try
            {
                db.SubmitChanges();
                existing = db.Currencies.FirstOrDefault(c => c.code == code);
            }
            catch (Exception e)
            {
                log.Error(string.Format("Error saving currency: {0} - {1}", code, name), e);
                return null;
            }
            return existing;
        }

        public IEnumerable<IRate> GetRatesHistory(ICurrency from, ICurrency to, DateTime since, DateTime upTo)
        {
            return db.Rates.Where(r => r.src == from.curID
                && r.dest == to.curID
                && r.day >= since && r.day <= upTo);
        }

        public IRate InsertOrUpdate(ICurrency @from, ICurrency to, DateTime day, double val)
        {
            var existing = GetRate(@from.curID, to.curID, day);
            if (existing == null)
            {
                var newRate = new Rate {src = @from.curID, dest = to.curID, val = val, day = day};
                log.Debug(string.Format("New exchange rate: {0}", newRate));
                db.Rates.InsertOnSubmit(newRate);
            }
            else if (SameValue(existing.val, val, 0.0000002f))
                return existing;
            else
            {
                existing.val = val;
                log.Debug(string.Format("Updating exhange rate: {0}", existing));
            }
            try
            {
                db.SubmitChanges();
                existing = GetRate(@from.curID, to.curID, day);
            }
            catch (Exception e)
            {
                log.Error("Error saving exchange rate", e);
                return null;
            }
            return existing;
        }

        private Rate GetRate(int src, int dest, DateTime day)
        {
            return db.Rates.FirstOrDefault(r => r.src == src && r.dest == dest && r.day == day);
        }

        private bool SameValue(double val1, double val2, double precition)
        {
            return Math.Abs(val1 - val2) < precition;
        }
    }
}
