using Exchange.Model;

namespace Exchange.Storage
{
    partial class Rate : IRate
    {
        public override string ToString()
        {
            return string.Format("{3:d} id:{0}->id:{1} = {2}", src, dest, val, day);
        }
    }

    partial class Currency : ICurrency
    {
        public override string ToString()
        {
            return code;
        }
    }
}
