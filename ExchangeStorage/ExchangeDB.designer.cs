﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Exchange.Storage
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Exchange")]
	public partial class ExchangeDBDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertCurrency(Currency instance);
    partial void UpdateCurrency(Currency instance);
    partial void DeleteCurrency(Currency instance);
    partial void InsertRate(Rate instance);
    partial void UpdateRate(Rate instance);
    partial void DeleteRate(Rate instance);
    #endregion
		
		public ExchangeDBDataContext() : 
				base(global::Exchange.Storage.Properties.Settings.Default.ExchangeConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public ExchangeDBDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ExchangeDBDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ExchangeDBDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public ExchangeDBDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Currency> Currencies
		{
			get
			{
				return this.GetTable<Currency>();
			}
		}
		
		public System.Data.Linq.Table<Rate> Rates
		{
			get
			{
				return this.GetTable<Rate>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Currencies")]
	public partial class Currency : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _curID;
		
		private string _code;
		
		private string _name;
		
		private EntitySet<Rate> _Rates;
		
		private EntitySet<Rate> _Rates1;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OncurIDChanging(int value);
    partial void OncurIDChanged();
    partial void OncodeChanging(string value);
    partial void OncodeChanged();
    partial void OnnameChanging(string value);
    partial void OnnameChanged();
    #endregion
		
		public Currency()
		{
			this._Rates = new EntitySet<Rate>(new Action<Rate>(this.attach_Rates), new Action<Rate>(this.detach_Rates));
			this._Rates1 = new EntitySet<Rate>(new Action<Rate>(this.attach_Rates1), new Action<Rate>(this.detach_Rates1));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_curID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int curID
		{
			get
			{
				return this._curID;
			}
			set
			{
				if ((this._curID != value))
				{
					this.OncurIDChanging(value);
					this.SendPropertyChanging();
					this._curID = value;
					this.SendPropertyChanged("curID");
					this.OncurIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_code", DbType="NChar(3) NOT NULL", CanBeNull=false)]
		public string code
		{
			get
			{
				return this._code;
			}
			set
			{
				if ((this._code != value))
				{
					this.OncodeChanging(value);
					this.SendPropertyChanging();
					this._code = value;
					this.SendPropertyChanged("code");
					this.OncodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_name", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string name
		{
			get
			{
				return this._name;
			}
			set
			{
				if ((this._name != value))
				{
					this.OnnameChanging(value);
					this.SendPropertyChanging();
					this._name = value;
					this.SendPropertyChanged("name");
					this.OnnameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Currency_Rate", Storage="_Rates", ThisKey="curID", OtherKey="dest")]
		public EntitySet<Rate> Rates
		{
			get
			{
				return this._Rates;
			}
			set
			{
				this._Rates.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Currency_Rate1", Storage="_Rates1", ThisKey="curID", OtherKey="src")]
		public EntitySet<Rate> Rates1
		{
			get
			{
				return this._Rates1;
			}
			set
			{
				this._Rates1.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_Rates(Rate entity)
		{
			this.SendPropertyChanging();
			entity.FromCurrency = this;
		}
		
		private void detach_Rates(Rate entity)
		{
			this.SendPropertyChanging();
			entity.FromCurrency = null;
		}
		
		private void attach_Rates1(Rate entity)
		{
			this.SendPropertyChanging();
			entity.ToCurrency = this;
		}
		
		private void detach_Rates1(Rate entity)
		{
			this.SendPropertyChanging();
			entity.ToCurrency = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Rates")]
	public partial class Rate : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _src;
		
		private int _dest;
		
		private double _val;
		
		private System.DateTime _day;
		
		private EntityRef<Currency> _FromCurrency;
		
		private EntityRef<Currency> _ToCurrency;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnsrcChanging(int value);
    partial void OnsrcChanged();
    partial void OndestChanging(int value);
    partial void OndestChanged();
    partial void OnvalChanging(double value);
    partial void OnvalChanged();
    partial void OndayChanging(System.DateTime value);
    partial void OndayChanged();
    #endregion
		
		public Rate()
		{
			this._FromCurrency = default(EntityRef<Currency>);
			this._ToCurrency = default(EntityRef<Currency>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_src", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int src
		{
			get
			{
				return this._src;
			}
			set
			{
				if ((this._src != value))
				{
					if (this._ToCurrency.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnsrcChanging(value);
					this.SendPropertyChanging();
					this._src = value;
					this.SendPropertyChanged("src");
					this.OnsrcChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_dest", DbType="Int NOT NULL", IsPrimaryKey = true)]
		public int dest
		{
			get
			{
				return this._dest;
			}
			set
			{
				if ((this._dest != value))
				{
					if (this._FromCurrency.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OndestChanging(value);
					this.SendPropertyChanging();
					this._dest = value;
					this.SendPropertyChanged("dest");
					this.OndestChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_val", DbType="Float NOT NULL")]
		public double val
		{
			get
			{
				return this._val;
			}
			set
			{
				if ((this._val != value))
				{
					this.OnvalChanging(value);
					this.SendPropertyChanging();
					this._val = value;
					this.SendPropertyChanged("val");
					this.OnvalChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_day", DbType="SmallDateTime NOT NULL", IsPrimaryKey=true)]
		public System.DateTime day
		{
			get
			{
				return this._day;
			}
			set
			{
				if ((this._day != value))
				{
					this.OndayChanging(value);
					this.SendPropertyChanging();
					this._day = value;
					this.SendPropertyChanged("day");
					this.OndayChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Currency_Rate", Storage="_FromCurrency", ThisKey="dest", OtherKey="curID", IsForeignKey=true)]
		public Currency FromCurrency
		{
			get
			{
				return this._FromCurrency.Entity;
			}
			set
			{
				Currency previousValue = this._FromCurrency.Entity;
				if (((previousValue != value) 
							|| (this._FromCurrency.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._FromCurrency.Entity = null;
						previousValue.Rates.Remove(this);
					}
					this._FromCurrency.Entity = value;
					if ((value != null))
					{
						value.Rates.Add(this);
						this._dest = value.curID;
					}
					else
					{
						this._dest = default(int);
					}
					this.SendPropertyChanged("FromCurrency");
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Currency_Rate1", Storage="_ToCurrency", ThisKey="src", OtherKey="curID", IsForeignKey=true)]
		public Currency ToCurrency
		{
			get
			{
				return this._ToCurrency.Entity;
			}
			set
			{
				Currency previousValue = this._ToCurrency.Entity;
				if (((previousValue != value) 
							|| (this._ToCurrency.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._ToCurrency.Entity = null;
						previousValue.Rates1.Remove(this);
					}
					this._ToCurrency.Entity = value;
					if ((value != null))
					{
						value.Rates1.Add(this);
						this._src = value.curID;
					}
					else
					{
						this._src = default(int);
					}
					this.SendPropertyChanged("ToCurrency");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
