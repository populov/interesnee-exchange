SELECT * FROM Rates WHERE src=1 AND dest=2 AND day BETWEEN '1.1.2013' AND '2.2.2013'

SELECT r.*, s.name AS srcName, d.name AS destName
 FROM Rates r
  INNER JOIN Currencies s ON r.src=s.curID
   INNER JOIN Currencies d ON r.dest=d.curID


SELECT [t0].[src], [t0].[dest], [t0].[val], [t0].[day]
FROM [dbo].[Rates] AS [t0]
WHERE ([t0].[src] = @p0) AND ([t0].[dest] = @p1) AND ([t0].[day] >= @p2) AND ([t0].[day] <= @p3)